# Notes

## Git commit types

-   _chore_: Regular code maintenance - e.g. package updates (_impact: none-small, frequency: common_)
-   _env_: Code environment changes, notes, comments (_impact: none, frequency: rare_)
-   _minor_: Code formatting, unused code removal (_impact: none, frequency: common_)
-   _refactor_: Re-write a section of code, without impacting functionality (_impact: none-small, frequency: rare_)
-   _fix_: Bug fixes
-   _feat_: New features
-   _style_: Updates to the app appearance/design/UX/UI

## Branches and releases

### Branch types

| Type                              | Description                                                     | Examples                                                                      | Prefix  |
| --------------------------------- | --------------------------------------------------------------- | ----------------------------------------------------------------------------- | ------- |
| New features                      | Visible changes which add functionality                         | Initial screen selection                                                      | `feat/` |
| "Background" features             | Invisible changes, which substantially affect how the app works | Migration to room                                                             | `feat/` |
| Bug fixes _(>1 commit)_           |                                                                 |                                                                               | `fix/`  |
| Environment changes _(>1 commit)_ |                                                                 | Migration to functional components, migration to Typescript, folder structure | `env/`  |

### Release workflow

The last commit of every release should be have a git tag with the version name.
This applies to both production and beta releases.

Production releases should always be made from the `master` branch.

**The `master` branch should only contain code that is ready for production.**
Therefore, other branches should only be merged into `master` after having been thoroughly tested and verified as fully working.

_Beta releases can be made from other branches as well. In fact, if these guidelines are followed, a beta release will never be made from the `master` branch, as it should contain production-ready code._

## Adding translations

### Steps

-   Update translation files
    -   Download the translation from POEditor
    -   Update country/chemical element names
-   Add the new language to every list where supported languages are specified
    -   LocalizationContext
    -   android/app/build.gradle -> android.defaultConfig.resConfigs
    -   RandomCountry
-   Add translator name to translators.json

## Bugs in libraries used

-   **react-native-screens _(>=3.10.1)_**: "Freeze" makes Settings screen freeze _(disabled)_
-   **@react-native-community/slider _(>=4.3.2)_**: causes a crash (value is sometimes set to zero, causing unexpected behavior)
