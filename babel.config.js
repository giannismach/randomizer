module.exports = {
    presets: ['module:metro-react-native-babel-preset'],
    env: {
        production: {
            plugins: ['transform-remove-console'],
        },
    },
    plugins: [
        [
            'module-resolver',
            {
                root: ['./'],
                extensions: [
                    '.ios.js',
                    '.android.js',
                    '.js',
                    '.json',
                    '.ts',
                    '.tsx',
                ],
                alias: {
                    assets: './src/assets',
                    components: './src/components',
                    context: './src/context',
                    hooks: './src/hooks',
                    modules: './src/modules',
                    navigation: './src/navigation',
                    screens: './src/screens',
                    utils: './src/utils',
                },
            },
        ],
        'react-native-reanimated/plugin',
    ],
};
