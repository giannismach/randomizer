package com.giannis.randomizer.db;

import androidx.room.Entity;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.annotation.NonNull;

@Entity(
    tableName="list_items", 
    foreignKeys = @ForeignKey(
        entity = UserList.class,
        parentColumns = "id",
        childColumns = "list_id",
        onDelete = ForeignKey.CASCADE // quite important - will break if removed
    )
)
public class ListItem {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @NonNull 
    public String item;
    
    @ColumnInfo(name = "list_id")
    @NonNull
    public long listId;

    @Ignore
    public ListItem(String item, long listId) {
        this.item = item;
        this.listId = listId;
    }

    public ListItem(int id, String item, long listId) {
        this.id = id;
        this.item = item;
        this.listId = listId;
    }
}