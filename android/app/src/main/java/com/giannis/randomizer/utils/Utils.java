package com.giannis.randomizer.utils;

import java.util.List;
import java.util.ArrayList;
import java.util.function.Function;

public class Utils {
    public static <I, O> List<O> transformList(List<I> list, Function<I, O> func) {
        List<O> newList = new ArrayList<>();
        for (int i = 0; i < list.size(); ++i) {
            newList.add(func.apply(list.get(i)));
        }
        return newList;
    }

    public static int objectToInt(Object obj) {
        return Double.valueOf(obj.toString()).intValue();
    }
}