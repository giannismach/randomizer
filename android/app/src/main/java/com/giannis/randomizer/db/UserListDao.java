package com.giannis.randomizer.db;

import com.giannis.randomizer.utils.Utils;

import androidx.room.Dao;
import androidx.room.Transaction;
import androidx.room.Query;
import androidx.room.Insert;
import androidx.room.Update;
import androidx.room.Delete;
import androidx.lifecycle.LiveData;

import java.util.List;

@Dao
public abstract class UserListDao {
    @Transaction
    @Query("select * from lists")
    public abstract LiveData<List<ListWithItems>> getLiveListsWithItems();

    @Transaction
    @Query("select * from lists")
    public abstract List<ListWithItems> getListsWithItems();

    @Insert
    public abstract long insertList(UserList list);

    @Insert
    public abstract void insertItem(ListItem item);

    @Insert 
    public abstract void insertItems(List<ListItem> items);

    @Transaction
    public void insertListWithItems(UserList list, List<String> items) {
        final long listId = insertList(list);
        insertItems(Utils.transformList(items, i -> new ListItem(i, listId)));
    }

    @Delete(entity = UserList.class)
    public abstract void deleteList(EntityId list);

    @Delete(entity = ListItem.class)
    public abstract void deleteItem(EntityId item);

    @Delete(entity = ListItem.class)
    public abstract void deleteItems(List<EntityId> items);

    @Update
    public abstract void updateList(UserList list);

    @Update
    public abstract void updateItem(ListItem item);
}