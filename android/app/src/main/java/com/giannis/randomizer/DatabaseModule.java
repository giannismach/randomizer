package com.giannis.randomizer;

import com.giannis.randomizer.db.*;
import com.giannis.randomizer.utils.Utils;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReadableArray;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.modules.core.DeviceEventManagerModule;

import androidx.lifecycle.Observer;
import androidx.annotation.Nullable;
import android.os.Handler;
import android.os.Looper;

import com.google.gson.Gson;

import java.lang.Double;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.function.Function;

public class DatabaseModule extends ReactContextBaseJavaModule {
    private static ReactApplicationContext reactContext;

    private static Observer<List<ListWithItems>> observer;

    private UserListDao mListDao;

    public DatabaseModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
        AppDatabase db = AppDatabase.getDatabase(context);
        mListDao = db.UserListDao();

        observer = new Observer<List<ListWithItems>>() {
            @Override
            public void onChanged(@Nullable final List<ListWithItems> lists) {
                if(!reactContext.hasActiveCatalystInstance() || lists == null) {
                    return;
                }
                String jsonLists = new Gson().toJson(lists);
                WritableMap params = Arguments.createMap();
                params.putString("data", jsonLists);
                reactContext.getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                            .emit("onListDataUpdate", params);
            }
        };

        new Handler(Looper.getMainLooper()).post(() -> {
            mListDao.getLiveListsWithItems().observeForever(observer);
        });
    }

    @Override
    public String getName() {
        return "Database";
    }

    @Override 
    public void onCatalystInstanceDestroy() {
        new Handler(Looper.getMainLooper()).post(() -> {
            mListDao.getLiveListsWithItems().removeObserver(observer);
        });
    }

    private ListItem readableMapToListItem(ReadableMap item) {
        return new ListItem(item.getString("item"), item.getInt("listId"));
    }

    private List<ListItem> readableArrayToListItems(ReadableArray items) {
        return Utils.transformList(
            Utils.transformList(items.toArrayList(), i -> Map.class.cast(i)),
            i -> new ListItem(i.get("item").toString(), Double.valueOf(i.get("listId").toString()).intValue())
        );
    }

    private List<EntityId> readableArrayToEntityIds(ReadableArray ids) {
        return Utils.transformList(ids.toArrayList(), i -> new EntityId(Utils.objectToInt(i)));
    }

    @ReactMethod
    public void getLists(Promise promise) {
        promise.resolve(new Gson().toJson(mListDao.getListsWithItems()));
    }

    @ReactMethod
    public void insertItem(ReadableMap item) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mListDao.insertItem(
                readableMapToListItem(item)
            );
        });
    }

    @ReactMethod
    public void insertItems(ReadableArray items) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mListDao.insertItems(
                readableArrayToListItems(items)
            );
        });
    }

    @ReactMethod
    public void insertListWithItems(String listName, ReadableArray items) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mListDao.insertListWithItems(
                new UserList(listName),
                Utils.transformList(items.toArrayList(), i -> i.toString())
            );
        });
    }

    @ReactMethod
    public void deleteList(Double id) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mListDao.deleteList(new EntityId(id.intValue()));
        });
    }

    @ReactMethod
    public void deleteItem(Double id) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mListDao.deleteItem(new EntityId(id.intValue()));
        });
    }

    @ReactMethod
    public void deleteItems(ReadableArray ids) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mListDao.deleteItems(
                readableArrayToEntityIds(ids)
            );
        });
    }

    @ReactMethod
    public void updateList(Double id, String name) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mListDao.updateList(new UserList(id.intValue(), name));
        });
    }

    @ReactMethod
    public void updateItem(Double id, Double listId, String item) {
        AppDatabase.databaseWriteExecutor.execute(() -> {
            mListDao.updateItem(new ListItem(id.intValue(), item, listId.intValue()));
        });
    }
}