package com.giannis.randomizer.db;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.Ignore;
import androidx.annotation.NonNull;

@Entity(tableName="lists")
public class UserList {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @NonNull
    public String name;

    @Ignore
    public UserList(String name) {
        this.name = name;
    }

    public UserList(int id, String name) {
        this.id = id;
        this.name = name;
    }
}