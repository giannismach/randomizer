package com.giannis.randomizer;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;

import com.giannis.randomizer.BuildConfig;

import java.util.Map;
import java.util.HashMap;

public class AppInfoModule extends ReactContextBaseJavaModule {
    AppInfoModule(ReactApplicationContext context) {
        super(context);
    }

    @Override
    public String getName() {
      return "AppInfo";
    }

    @Override
    public Map<String, Object> getConstants() {
      final Map<String, Object> constants = new HashMap<>();
      constants.put("appVersion", BuildConfig.VERSION_NAME);
      return constants;
    }
}