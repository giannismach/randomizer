package com.giannis.randomizer;

import com.facebook.react.bridge.ContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import android.content.ClipboardManager;
import android.content.ClipData;
import android.content.Context;

public class CustomClipboardModule extends ContextBaseJavaModule {
    CustomClipboardModule(Context context) {
        super(context);
    }

    @Override
    public String getName() {
        return "CustomClipboard";
    }

    private ClipboardManager getClipboardService() {
        return (ClipboardManager) getContext().getSystemService(getContext().CLIPBOARD_SERVICE);
    }

    @ReactMethod
    public void setString(String text) {
        ClipData clipdata = ClipData.newPlainText(null, text);
        ClipboardManager clipboard = getClipboardService();
        clipboard.setPrimaryClip(clipdata);
    }
}