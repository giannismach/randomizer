package com.giannis.randomizer;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.BaseActivityEventListener;

import android.content.Intent;
import android.app.Activity;
import android.net.Uri;
import android.os.ParcelFileDescriptor;

import java.util.Objects;

import java.lang.StringBuilder;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileSystemModule extends ReactContextBaseJavaModule {

    private static final int WRITE_FILE = 1;
    private static final int SELECT_FILE = 2;

    private static final String E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST";
    private static final String E_PICKER_CANCELLED = "E_PICKER_CANCELLED";
    private static final String E_FAILED_TO_SHOW_PICKER = "E_FAILED_TO_SHOW_PICKER";
    private static final String E_NO_DATA_FOUND = "E_NO_DATA_FOUND";

    private static ReactApplicationContext reactContext;

    private Promise mCreateFilePromise;
    private Promise mSelectFilePromise;
    private String fileContent;

    private final ActivityEventListener mActivityEventListener = new BaseActivityEventListener() {
        @Override
        public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent intent) {
            if (requestCode == WRITE_FILE) {
                if (mCreateFilePromise != null) {
                    if (resultCode == Activity.RESULT_CANCELED) {
                        mCreateFilePromise.reject(E_PICKER_CANCELLED, "Operation cancelled");
                    } else if (resultCode == Activity.RESULT_OK) {
                        Uri uri = intent.getData();

                        if (uri == null) {
                            mCreateFilePromise.reject(E_NO_DATA_FOUND, "No data found");
                        } else {
                            editFile(uri);
                            mCreateFilePromise.resolve(uri.toString());
                        }
                    }
                    mCreateFilePromise = null;
                } else {
                    if (resultCode == Activity.RESULT_OK) {
                        Uri uri = intent.getData();
                        if (uri != null) {
                            editFile(uri);
                        }
                    }
                }
            } else if (requestCode == SELECT_FILE) {
                if (mSelectFilePromise != null) {
                    if (resultCode == Activity.RESULT_CANCELED) {
                        mSelectFilePromise.reject(E_PICKER_CANCELLED, "Operation cancelled");
                    } else if (resultCode == Activity.RESULT_OK) {
                        Uri uri = intent.getData();
                        if (uri == null) {
                            mSelectFilePromise.reject(E_NO_DATA_FOUND, "No data found");
                        } else {
                            try {
                                mSelectFilePromise.resolve(readFile(uri));  
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        }
    };

    FileSystemModule(ReactApplicationContext context) {
        super(context);
        reactContext = context;
        reactContext.addActivityEventListener(mActivityEventListener);
    }

    @Override
    public String getName() {
      return "FileSystem";
    }

    private void editFile(Uri uri) {
        try {
            ParcelFileDescriptor pfd = getCurrentActivity().getContentResolver().openFileDescriptor(uri, "w");
            FileOutputStream fileOutputStream = new FileOutputStream(pfd.getFileDescriptor());
            fileOutputStream.write(fileContent.getBytes());
            fileOutputStream.close();
            pfd.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    };

    private String readFile(Uri uri) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStream inputStream =
                getCurrentActivity().getContentResolver().openInputStream(uri);
                BufferedReader reader = new BufferedReader(
                new InputStreamReader(Objects.requireNonNull(inputStream)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        return stringBuilder.toString();
    }

    @ReactMethod
    private void createFile(String mimeType, String fileName, String content, final Promise promise) {
        Activity currentActivity = getCurrentActivity();

        if (currentActivity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }

        mCreateFilePromise = promise;
        fileContent = content;

        try {
            Intent intent = new Intent(Intent.ACTION_CREATE_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType(mimeType);
            intent.putExtra(Intent.EXTRA_TITLE, fileName);
            currentActivity.startActivityForResult(intent, WRITE_FILE);
        } catch (Exception e) {
            mCreateFilePromise.reject(E_FAILED_TO_SHOW_PICKER, e);
            mCreateFilePromise = null;
        }
    }

    @ReactMethod
    private void selectFile(String mimeType, final Promise promise) {
        Activity currentActivity = getCurrentActivity();

        if (currentActivity == null) {
            promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist");
            return;
        }

        mSelectFilePromise = promise;

        try {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType(mimeType);
            currentActivity.startActivityForResult(intent, SELECT_FILE);
        } catch (Exception e) {
            mSelectFilePromise.reject(E_FAILED_TO_SHOW_PICKER, e);
            mSelectFilePromise = null;
        }
        
        
    }
}