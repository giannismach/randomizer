package com.giannis.randomizer.db;

import androidx.room.Embedded;
import androidx.room.Relation;

import java.util.List;

public class ListWithItems {
    @Embedded public UserList list;

    @Relation(
        parentColumn = "id",
        entityColumn = "list_id"
    )
    public List<ListItem> items;
}