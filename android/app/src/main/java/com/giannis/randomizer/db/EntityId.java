package com.giannis.randomizer.db;

// a helper class which only includes an arbitrary entity id
// useful for entity deletions

public class EntityId {
    public int id;

    public EntityId(int id) {
        this.id = id;
    }
}