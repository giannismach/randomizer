import 'react-native-gesture-handler';

import {AppRegistry} from 'react-native';
// import {enableFreeze} from 'react-native-screens';
import App from './src/App';

// "freeze" makes Settings screen non-responsive using react-native-screens@3.10.1
// enableFreeze(true);

AppRegistry.registerComponent('Randomizer', () => App);
