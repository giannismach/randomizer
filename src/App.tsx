import React, {useEffect, useMemo, useCallback} from 'react';
import {Platform, UIManager, StatusBar, Appearance} from 'react-native';
import {
    adaptNavigationTheme,
    Provider as PaperProvider,
} from 'react-native-paper';
import {
    NavigationContainer,
    DefaultTheme as LightNavTheme,
    DarkTheme as DarkNavTheme,
} from '@react-navigation/native';

import MainNavigator from 'navigation/MainNavigator';

import {ListsContextProvider} from 'context/ListsContext';
import {LocalizationProvider} from 'context/LocalizationContext';
import PreferencesContext, {Theme} from 'context/PreferencesContext';
import useAppPreference from 'hooks/useAppPreference';
import {DrawerScreenRouteName} from 'navigation/types';
import themes from 'utils/themes';

if (Platform.OS === 'android') {
    if (UIManager.setLayoutAnimationEnabledExperimental) {
        UIManager.setLayoutAnimationEnabledExperimental(true);
    }
}

StatusBar.setTranslucent(true);
StatusBar.setBackgroundColor('#0004');

const navThemes = adaptNavigationTheme({
    materialLight: themes.light,
    materialDark: themes.dark,
    reactNavigationLight: LightNavTheme,
    reactNavigationDark: DarkNavTheme,
});

const App = () => {
    const [theme, setTheme, loadingTheme] = useAppPreference<Theme>({
        defaultValue: Theme.Light,
        skip: Platform.Version >= 29,
        asyncStorageKey: '@Randomizer_theme',
    });

    const [initialScreen, setInitialScreen, loadingInitialScreen] =
        useAppPreference<DrawerScreenRouteName>({
            defaultValue: 'Number',
            asyncStorageKey: '@Randomizer_initialScreen',
        });

    const toggleTheme = useCallback(() => {
        setTheme(t => (t === Theme.Dark ? Theme.Light : Theme.Dark));
    }, [setTheme]);

    useEffect(() => {
        const onSystemThemeChange: Appearance.AppearanceListener = ({
            colorScheme,
        }) => {
            if (colorScheme === Theme.Light || colorScheme === Theme.Dark) {
                setTheme(colorScheme as Theme);
            }
        };

        if (Platform.Version >= 29) {
            const colorScheme = Appearance.getColorScheme();
            onSystemThemeChange({colorScheme});

            const listener = Appearance.addChangeListener(onSystemThemeChange);

            return () => {
                listener.remove();
            };
        }
    }, [setTheme]);

    const preferencesContext = useMemo(
        () => ({
            theme,
            toggleTheme,
            initialScreen,
            setInitialScreen,
        }),
        [initialScreen, setInitialScreen, theme, toggleTheme]
    );

    const materialTheme = useMemo(() => themes[theme], [theme]);
    const navTheme = useMemo(
        () =>
            theme === Theme.Dark ? navThemes.DarkTheme : navThemes.LightTheme,
        [theme]
    );

    if (loadingTheme || loadingInitialScreen) {
        return null;
    }

    return (
        <LocalizationProvider>
            <PreferencesContext.Provider value={preferencesContext}>
                <ListsContextProvider>
                    <PaperProvider theme={materialTheme}>
                        <NavigationContainer theme={navTheme}>
                            <MainNavigator />
                        </NavigationContainer>
                    </PaperProvider>
                </ListsContextProvider>
            </PreferencesContext.Provider>
        </LocalizationProvider>
    );
};

export default App;
