import {useCallback} from 'react';

const useRequestAnimationFrame = (
    func: (...args: any[]) => void,
    ...args: any[]
) => {
    return useCallback(() => {
        requestAnimationFrame(() => {
            func(...args);
        });
    }, [args, func]);
};

export default useRequestAnimationFrame;
