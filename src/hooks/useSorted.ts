import React from 'react';

const useSorted = <T>(arr: T[], process: (val: T) => string) => {
    const sortedVal = React.useMemo(
        () => arr.sort((a, b) => process(a).localeCompare(process(b))),
        [arr, process]
    );
    return sortedVal;
};

export default useSorted;
