import {useWindowDimensions} from 'react-native';

const useScreenOrientation = () => {
    const {height, width} = useWindowDimensions();
    return width > height ? 'landscape' : 'portrait';
};

export default useScreenOrientation;
