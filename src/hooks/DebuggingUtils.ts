import {useEffect} from 'react';

namespace DebuggingUtils {
    export const useRenderLog = (component: string) => {
        useEffect(() => {
            if (__DEV__) {
                console.log(`${component} rendered`);
            }
        });
    };

    export const useValueChangeLog = (value: any, label?: string) => {
        useEffect(() => {
            if (__DEV__) {
                if (label) {
                    console.log(label, value);
                } else {
                    console.log(value);
                }
            }
        }, [label, value]);
    };
}

export default DebuggingUtils;
