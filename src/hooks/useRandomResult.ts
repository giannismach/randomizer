import React, {useState, useRef, useCallback} from 'react';

const useRandomResult = <T>({
    initialValue,
    beforeUpdate,
    afterUpdate,
    generateResult,
    animation,
}: {
    initialValue: T;
    beforeUpdate?: () => boolean;
    afterUpdate?: () => void;
    generateResult: () => T;
    animation?: (ref: React.Ref<any>) => void;
}) => {
    const [result, setResult] = useState(initialValue);

    const ref = useRef<any>();

    const update = () => {
        if (beforeUpdate?.() ?? true) {
            setResult(generateResult());
            if (afterUpdate) {
                afterUpdate();
            }
            if (animation) {
                animation(ref);
            }
        }
    };

    const reset = useCallback(() => {
        setResult(initialValue);
    }, [initialValue]);

    return {result, ref, update, reset};
};

export default useRandomResult;
