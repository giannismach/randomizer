import React, {useState, useEffect, useMemo} from 'react';
import {InteractionManager} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {StringConverter} from 'utils/Converters';
import {StringValidator} from 'utils/Validators';

/**
 * @summary A utility hook for local option management.
 * @description Handles option persistence.
 *
 * @param initialValue Default value for the option.
 * @param asyncStorageKey The key under which the option value should be saved in `AsyncStorage`. If not defined, the value will not be persisted.
 * @param converter A function which will convert the string value retrieved from `AsyncStorage` to the valid option type.
 * @param validator A function which will ensure that the value retrieved from `AsyncStorage` is valid.
 *
 * @returns [currentValue, setValue, initialValue]
 * Initial value can be used in uncontrolled components.
 */
const useOption = <T extends string | number | boolean | Date>({
    defaultValue,
    asyncStorageKey,
    converter,
    validator,
}: {
    defaultValue: T;
    asyncStorageKey?: string;
    converter?: StringConverter<T>;
    validator?: StringValidator;
}): [T, React.Dispatch<React.SetStateAction<T>>, T] => {
    const [option, setOption] = useState(defaultValue);
    const [initialValue, setInitialValue] = useState(defaultValue);
    const stringified = useMemo(() => {
        return option.toString();
    }, [option]);

    useEffect(() => {
        if (asyncStorageKey) {
            InteractionManager.runAfterInteractions(() => {
                try {
                    AsyncStorage.setItem(asyncStorageKey, stringified);
                } catch {}
            });
        }
    }, [asyncStorageKey, stringified]);

    useEffect(() => {
        const parse = (str: string) => converter?.(str) ?? (str as T);
        const validate = (str: string) => validator?.(str) ?? true;

        if (asyncStorageKey) {
            AsyncStorage.getItem(asyncStorageKey).then(v => {
                if (v && validate(v)) {
                    setOption(parse(v));
                    setInitialValue(parse(v));
                }
            });
        }
    }, [asyncStorageKey, converter, validator]);

    return [option, setOption, initialValue];
};

export default useOption;
