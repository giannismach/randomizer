import {useState, useCallback} from 'react';

const useBooleanState = (
    initialValue: boolean = false
): [boolean, () => void, () => void, () => void] => {
    const [value, setValue] = useState(initialValue);

    const enable = useCallback(() => setValue(true), []);
    const disable = useCallback(() => setValue(false), []);
    const toggle = useCallback(() => setValue(v => !v), []);
    return [value, enable, disable, toggle];
};

export default useBooleanState;
