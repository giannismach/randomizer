import React from 'react';
import {NativeEventEmitter, NativeModules} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useListsContext} from 'context/ListsContext';
import Database from 'modules/Database';
import {ListWithItems, ListID, ListActions} from 'utils/types';

namespace ListHelpers {
    /**
     * Should only be used once, in ListsContext.
     * Components should not use this, since it would register another listener. They should access list data from ListsContext instead.
     * @returns an array of ListWithItems
     */
    export const useListData = (): ListWithItems[] => {
        const [data, setData] = React.useState<ListWithItems[]>([]);

        React.useEffect(() => {
            const listener = ({data: newData}: {data: string}) => {
                setData(JSON.parse(newData));
            };

            const eventEmitter = new NativeEventEmitter(NativeModules.Database);
            const eventListener = eventEmitter.addListener(
                'onListDataUpdate',
                listener
            );
            return () => {
                eventListener.remove();
            };
        }, []);

        React.useEffect(() => {
            Database.getLists().then(setData);
        }, []);

        return data;
    };

    const keyFromName = (name: string) => `@Randomizer_userLists_${name}`;
    const nameFromKey = (key: string) => {
        const ind = key.search(/(?<=^@Randomizer_userLists_).+$/);
        return key.substring(ind);
    };

    /**
     * @summary Migrate from AsyncStorage to Sqlite (if needed)
     */
    export const useAsyncStorageToSqliteMigration = () => {
        React.useEffect(() => {
            let names: string[] = [];
            AsyncStorage.getItem('@Randomizer_listNames')
                .then(res => {
                    if (res) {
                        names = JSON.parse(res);
                        return AsyncStorage.multiGet(names.map(keyFromName));
                    }
                })
                .then(res => {
                    if (res) {
                        for (const [key, value] of res) {
                            if (value) {
                                Database.insertListWithItems(
                                    nameFromKey(key),
                                    JSON.parse(value)
                                );
                            }
                        }
                        // 1 - to avoid re-inserting the lists when the effect is run again
                        // 2 - it's redundant data, no reason why it shouldn't be removed
                        return AsyncStorage.multiRemove([
                            '@Randomizer_listNames',
                            ...names.map(keyFromName),
                        ]);
                    }
                });
        }, []);
    };

    /**
     * @summary Get a (memoized) list from its id.
     * @param listId the id of the list
     */
    export const useListFromId = (listId: ListID | null) => {
        const {data} = useListsContext();
        const list = React.useMemo(
            () => data.find(({list: {id}}) => id === listId),
            [data, listId]
        );

        if (list === undefined) {
            throw Error('List not found with requested id');
        }
        return list;
    };

    export const useListActions = (listId: number): ListActions => {
        const remove = React.useCallback(() => {
            Database.deleteList(listId);
        }, [listId]);

        const addItem = React.useCallback(
            (item: string) => {
                Database.insertItem({item, listId});
            },
            [listId]
        );

        const updateItem = React.useCallback(
            (itemId: number, item: string) => {
                Database.updateItem(itemId, listId, item);
            },
            [listId]
        );

        const deleteItem = React.useCallback((itemId: number) => {
            Database.deleteItem(itemId);
        }, []);

        const updateName = React.useCallback(
            (newName: string) => {
                Database.updateList(listId, newName);
            },
            [listId]
        );

        return {
            remove,
            addItem,
            updateItem,
            deleteItem,
            updateName,
        };
    };
}

export default ListHelpers;
