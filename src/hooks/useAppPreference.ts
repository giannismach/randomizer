import React, {useState, useEffect} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

const useAppPreference = <T extends string>({
    defaultValue,
    skip = false,
    asyncStorageKey,
}: {
    defaultValue: T;
    skip?: boolean;
    asyncStorageKey?: string;
}): [T, React.Dispatch<React.SetStateAction<T>>, boolean] => {
    const [value, setValue] = useState(defaultValue);
    const [loading, setLoading] = useState(!skip && !!asyncStorageKey);

    // restore saved value on mount
    useEffect(() => {
        if (!skip && asyncStorageKey) {
            AsyncStorage.getItem(asyncStorageKey).then(v => {
                if (v) {
                    setValue(v as T);
                }
                setLoading(false);
            });
        }
    }, [asyncStorageKey, skip]);

    // store value on change
    useEffect(() => {
        if (!skip && asyncStorageKey) {
            AsyncStorage.setItem(asyncStorageKey, value);
        }
    }, [asyncStorageKey, skip, value]);

    return [value, setValue, loading];
};

export default useAppPreference;
