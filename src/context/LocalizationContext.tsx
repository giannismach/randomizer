import React, {useState, useCallback, useEffect, useMemo} from 'react';
import * as RNLocalize from 'react-native-localize';

import contextWithNullSecurity from 'utils/contextWithNullSecurity';

import en from 'assets/strings/translations/en.json';
import el from 'assets/strings/translations/el.json';
import es from 'assets/strings/translations/es.json';
import pt from 'assets/strings/translations/pt.json';
import ru from 'assets/strings/translations/ru.json';
import bg from 'assets/strings/translations/bg.json';
import fr from 'assets/strings/translations/fr.json';
import he from 'assets/strings/translations/he.json';
import de from 'assets/strings/translations/de.json';
import pl from 'assets/strings/translations/pl.json';
import { AppState, AppStateStatus } from 'react-native';

export type Locale =
    | 'en'
    | 'el'
    | 'es'
    | 'pt'
    | 'ru'
    | 'bg'
    | 'fr'
    | 'he'
    | 'de'
    | 'pl';

export type StringKey = keyof (typeof en &
    typeof el &
    typeof es &
    typeof pt &
    typeof ru &
    typeof bg &
    typeof fr &
    typeof he &
    typeof de &
    typeof pl);

const strings: Record<Locale, Record<StringKey, string>> = {
    en,
    el,
    es,
    pt,
    ru,
    bg,
    fr,
    he,
    de,
    pl,
};

const supportedLocales: Locale[] = [
    'en',
    'el',
    'es',
    'pt',
    'ru',
    'bg',
    'fr',
    'he',
    'de',
    'pl',
];
const defaultLocale: Locale = 'en';

const alternativeLanguageCodes: Record<string, Locale> = {
    iw: 'he',
};

const getLocale = () => {
    const {languageCode, isRTL} = RNLocalize.getLocales()[0];
    if (supportedLocales.includes(languageCode as Locale)) {
        return {languageCode: languageCode as Locale, isRTL};
    } else if (Object.keys(alternativeLanguageCodes).includes(languageCode)) {
        return {languageCode: alternativeLanguageCodes[languageCode], isRTL};
    } else {
        return {
            languageCode: defaultLocale,
            isRTL: false,
        };
    }
};

const useCurrentDeviceLocale = () => {
    const [locale, setLocale] = useState(() => getLocale());

    useEffect(() => {
        const onAppStateChange = (state: AppStateStatus) => {
            if (state === 'active') {
                const _locale = getLocale();
                setLocale(_locale);
            }
        };

        AppState.addEventListener('change', onAppStateChange);
    }, []);

    return locale;
};

interface IContext {
    languageCode: Locale;
    isRTL: boolean;
    translate: (key: StringKey) => string;
}

const LocalizationContext = React.createContext<IContext | null>(null);
LocalizationContext.displayName = 'LocalizationContext';

const LocalizationProvider = (props: {children: React.ReactNode}) => {
    const {languageCode, isRTL} = useCurrentDeviceLocale();

    const translate = useCallback(
        (key: StringKey) => strings[languageCode][key],
        [languageCode]
    );

    const context = useMemo(
        () => ({languageCode, isRTL, translate}),
        [isRTL, languageCode, translate]
    );

    return (
        <LocalizationContext.Provider value={context}>
            {props.children}
        </LocalizationContext.Provider>
    );
};

const useLocalization = contextWithNullSecurity(LocalizationContext);

export {LocalizationProvider, useLocalization};
