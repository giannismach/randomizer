import React from 'react';
import ListHelpers from 'hooks/ListHelpers';
import contextWithNullSecurity from 'utils/contextWithNullSecurity';
import {ListWithItems, ListActions} from 'utils/types';

const SelectedListContext = React.createContext<
    (ListWithItems & {actions: ListActions}) | null
>(null);
SelectedListContext.displayName = 'SelectedListContext';

export const SelectedListProvider = (props: {
    listId: number;
    children: React.ReactNode;
}) => {
    const list = ListHelpers.useListFromId(props.listId);
    const actions = ListHelpers.useListActions(props.listId);

    // temporary solution to avoid crash in EditList if empty list items exist
    React.useEffect(() => {
        list.items.forEach(item => {
            if (item.item.length === 0) {
                actions.deleteItem(item.id);
            }
        });
    }, [actions.deleteItem, list.items, actions]);

    return (
        <SelectedListContext.Provider
            value={{
                ...list,
                actions,
            }}>
            {props.children}
        </SelectedListContext.Provider>
    );
};

export const useSelectedList = contextWithNullSecurity(SelectedListContext);
