import React from 'react';
import {DrawerScreenRouteName} from 'navigation/types';
import contextWithNullSecurity from 'utils/contextWithNullSecurity';

export enum Theme {
    Light = 'light',
    Dark = 'dark',
}

interface IContext {
    theme: Theme;
    toggleTheme: () => void;
    initialScreen: DrawerScreenRouteName;
    setInitialScreen: React.Dispatch<
        React.SetStateAction<DrawerScreenRouteName>
    >;
}

const PreferencesContext = React.createContext<IContext | null>(null);
PreferencesContext.displayName = 'PreferencesContext';

const usePreferences = contextWithNullSecurity(PreferencesContext);

export default PreferencesContext;
export {usePreferences};
