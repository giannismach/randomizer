import React, {useMemo, useCallback} from 'react';
import {useTheme} from 'react-native-paper';
import Snackbar from 'react-native-snackbar';
import AsyncStorage from '@react-native-async-storage/async-storage';
import ListHelpers from 'hooks/ListHelpers';
import Database from 'modules/Database';
import FileSystem from 'modules/FileSystem';
import {ListWithItems} from 'utils/types';
import contextWithNullSecurity from 'utils/contextWithNullSecurity';
import {useLocalization} from './LocalizationContext';

namespace ActionTypes {
    export type Create = (
        name: string,
        items: string[],
        onComplete: () => void
    ) => void;

    export type ImportFromBackup = (
        onStart: () => void,
        onEnd: () => void
    ) => void;

    export type ExportToBackup = () => void;
}

interface IContext {
    data: ListWithItems[];
    getListFromId: (id: number) => ListWithItems;
    actions: {
        create: ActionTypes.Create;
        importFromBackup: ActionTypes.ImportFromBackup;
        exportToBackup: ActionTypes.ExportToBackup;
    };
}

const ListsContext = React.createContext<IContext | null>(null);
ListsContext.displayName = 'ListsContext';

const ListsContextProvider = (props: {children: React.ReactNode}) => {
    const data = ListHelpers.useListData();

    ListHelpers.useAsyncStorageToSqliteMigration();

    const create: ActionTypes.Create = useCallback(
        (name, items, onComplete) => {
            Database.insertListWithItems(name, items);
            onComplete();
        },
        []
    );

    const getListFromId = useCallback(
        (listId: number) => {
            const list = data.find(({list: {id}}) => id === listId);
            if (list === undefined) {
                throw Error('List with requested id not found');
            }
            return list;
        },
        [data]
    );

    const {translate} = useLocalization();
    const theme = useTheme();

    const showErrorSnackbar = useCallback(() => {
        Snackbar.show({
            text: translate('anErrorOccured'),
            duration: Snackbar.LENGTH_SHORT,
            backgroundColor: theme.colors.error,
        });
    }, [theme.colors.error, translate]);

    const importFromBackup: ActionTypes.ImportFromBackup = useCallback(
        (onStart, onEnd) => {
            return FileSystem.selectFile('text/plain')
                .then((fileContent: string) => {
                    onStart();
                    let newItems: Record<string, string[]> = JSON.parse(
                        fileContent.trim()
                    );
                    let newNames = Object.keys(newItems).filter(name => {
                        return !!newItems[name];
                    });

                    // alter list names (if needed) to prevent duplicates
                    let allNamesResolved = data.map(l => l.list.name);
                    for (let name of newNames) {
                        let count = 1;
                        let newName = name;
                        while (allNamesResolved.indexOf(newName) !== -1) {
                            const isDuplicate =
                                newName.match(/^(.+) \((\d+)\)$/);
                            if (isDuplicate) {
                                count = Number(isDuplicate[2]) + 1;
                                newName = `${isDuplicate[1]} (${count++})`;
                            } else {
                                newName = `${name} (${count++})`;
                            }
                        }
                        const nameInd = newNames.indexOf(name);
                        newNames[nameInd] = newName;

                        const items = [...{...newItems}[name]];
                        delete newItems[name];
                        newItems[newName] = items;
                        allNamesResolved.push(newName);
                    }

                    newNames.forEach(name => {
                        Database.insertListWithItems(name, newItems[name]);
                    });
                })
                .then(() => {
                    onEnd();
                    Snackbar.show({
                        text: translate('listsImportedSuccessfully'),
                        duration: Snackbar.LENGTH_SHORT,
                    });
                })
                .catch((err: Error) => {
                    if (err.message !== 'Operation cancelled') {
                        // probably invalid backup file format
                        showErrorSnackbar();
                        console.log(err);
                    }
                })
                .finally(onEnd);
        },
        [data, showErrorSnackbar, translate]
    );

    const exportToBackup: ActionTypes.ExportToBackup = useCallback(() => {
        const mimeType = 'text/plain';

        const today = new Date();
        const fileName = `Randomizer-Backup-${today.getFullYear()}-${
            today.getMonth() + 1
        }-${today.getDate()}.txt`;

        const fileContent = JSON.stringify(
            data.reduce(
                (acc, cur) => ({
                    ...acc,
                    [cur.list.name]: cur.items.map(({item}) => item),
                }),
                {}
            )
        );

        FileSystem.createFile(mimeType, fileName, fileContent)
            .then(() => {
                AsyncStorage.setItem(
                    'lastBackupGeneration',
                    JSON.stringify(new Date())
                ); // currently unused
                Snackbar.show({
                    text: translate('backupFileSavedSuccessfully'),
                    duration: Snackbar.LENGTH_SHORT,
                });
            })
            .catch((err: Error) => {
                if (err.message !== 'Operation cancelled') {
                    showErrorSnackbar();
                }
            });
    }, [data, showErrorSnackbar, translate]);

    const context: IContext = useMemo(
        () => ({
            data,
            getListFromId,
            actions: {
                create,
                importFromBackup,
                exportToBackup,
            },
        }),
        [create, data, exportToBackup, importFromBackup, getListFromId]
    );

    return (
        <ListsContext.Provider value={context}>
            {props.children}
        </ListsContext.Provider>
    );
};

const useListsContext = contextWithNullSecurity(ListsContext);

export {useListsContext, ListsContextProvider};
