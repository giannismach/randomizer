import {NativeModules} from 'react-native';
import {ListID, ListWithItems, ListItemID, ListItem} from 'utils/types';

type ListItemWithoutId = Pick<ListItem, 'item' | 'listId'>;
namespace Database {
    export const getLists = async (): Promise<ListWithItems[]> => {
        const data: string = await NativeModules.Database.getLists();
        return JSON.parse(data);
    };

    export const insertItem = (item: ListItemWithoutId) => {
        NativeModules.Database.insertItem(item);
    };

    export const insertItems = (items: ListItemWithoutId[]): void => {
        NativeModules.Database.insertItems(items);
    };

    export const insertListWithItems = (name: string, items: string[]) => {
        NativeModules.Database.insertListWithItems(name, items);
    };

    export const deleteList = (id: ListID): void => {
        NativeModules.Database.deleteList(id);
    };

    export const deleteItem = (id: ListItemID) => {
        NativeModules.Database.deleteItem(id);
    };

    export const deleteItems = (ids: ListItemID[]) => {
        NativeModules.Database.deleteItems(ids);
    };

    export const updateList = (id: ListID, name: string) => {
        NativeModules.Database.updateList(id, name);
    };

    export const updateItem = (
        id: ListItemID,
        listId: ListID,
        item: string
    ) => {
        NativeModules.Database.updateItem(id, listId, item);
    };
}

export default Database;
