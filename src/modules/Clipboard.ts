import {NativeModules} from 'react-native';

namespace Clipboard {
    export const setString = (str: string) => {
        NativeModules.CustomClipboard.setString(str);
    };
}

export default Clipboard;
