import {NativeModules} from 'react-native';

namespace FileSystem {
    export const selectFile = (mimeType: string): Promise<string> => {
        return NativeModules.FileSystem.selectFile(mimeType);
    };

    export const createFile = (
        mimeType: string,
        fileName: string,
        fileContent: string
    ): Promise<string> => {
        return NativeModules.FileSystem.createFile(
            mimeType,
            fileName,
            fileContent
        );
    };
}

export default FileSystem;
