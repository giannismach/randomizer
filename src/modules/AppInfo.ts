import {NativeModules} from 'react-native';

namespace AppInfo {
    export const {appVersion}: {appVersion: string} =
        NativeModules.AppInfo.getConstants();
}

export default AppInfo;
