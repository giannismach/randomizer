import React, {useCallback} from 'react';
import {StatusBar, Alert} from 'react-native';
import {Appbar} from 'react-native-paper';
import {RouteProp} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';

import ListElement from 'screens/ListElement';
import ShuffleList from 'screens/ShuffleList';
import Groups from 'screens/Groups';
import EditList from 'screens/EditList';

import ListSelectionDialog from 'components/ListSelectionDialog';
import {useLocalization} from 'context/LocalizationContext';
import {SelectedListProvider} from 'context/SelectedListContext';
import ListHelpers from 'hooks/ListHelpers';
import useBooleanState from 'hooks/useBooleanState';
import {MainStackParamList, ListMainTabsParamList} from 'navigation/types';
import {ListWithItems} from 'utils/types';

const Tab = createMaterialBottomTabNavigator<ListMainTabsParamList>();

type Props = {
    navigation: NativeStackNavigationProp<MainStackParamList, 'ListMain'>;
    route: RouteProp<MainStackParamList, 'ListMain'>;
};

const ListMainTabs = (props: Props) => {
    const {translate} = useLocalization();

    const {list, items} = ListHelpers.useListFromId(props.route.params.listId);
    const actions = ListHelpers.useListActions(props.route.params.listId);

    const [dialogVisible, showDialog, hideDialog] = useBooleanState();

    const deleteList = useCallback(() => {
        actions.remove();
        props.navigation.popToTop();
    }, [actions, props.navigation]);

    const onDelete = useCallback(() => {
        Alert.alert(
            translate('confirmDeletion'),
            translate('deleteList'),
            [
                {
                    text: translate('cancel'),
                    style: 'cancel',
                },
                {
                    text: 'OK',
                    style: 'destructive',
                    onPress: deleteList,
                },
            ],
            {
                cancelable: true,
            }
        );
    }, [deleteList, translate]);

    const changeSelectedList = React.useCallback(
        ({list: newList}: ListWithItems) => {
            props.navigation.setParams({
                listId: newList.id,
            });
            hideDialog();
        },
        [props.navigation, hideDialog]
    );

    return (
        <>
            <Appbar.Header statusBarHeight={StatusBar.currentHeight} elevated>
                <Appbar.BackAction onPress={props.navigation.goBack} />
                <Appbar.Content title={list.name} onPress={showDialog} />
                <Appbar.Action
                    icon="delete-outline"
                    onPress={onDelete}
                    accessibilityLabel="Delete list"
                />
            </Appbar.Header>
            <SelectedListProvider listId={props.route.params.listId}>
                <Tab.Navigator backBehavior="none" shifting={false}>
                    <Tab.Screen
                        name="Element"
                        options={{
                            tabBarLabel:
                                translate('element')[0].toUpperCase() +
                                translate('element').substring(1), // TODO: change
                            tabBarIcon: 'format-list-bulleted-type',
                        }}
                        component={ListElement}
                    />
                    <Tab.Screen
                        name="Shuffle"
                        options={{
                            tabBarLabel: translate('shuffle'),
                            tabBarIcon: 'shuffle-variant',
                        }}
                        component={ShuffleList}
                    />
                    <Tab.Screen
                        name="Groups"
                        options={{
                            tabBarLabel: translate('groups'),
                            tabBarIcon: 'account-multiple',
                        }}
                        component={Groups}
                    />
                    <Tab.Screen
                        name="Edit"
                        options={{
                            tabBarLabel: translate('edit'),
                            tabBarIcon: 'pencil',
                        }}
                        component={EditList}
                    />
                </Tab.Navigator>
            </SelectedListProvider>
            <ListSelectionDialog
                visible={dialogVisible}
                onDismiss={hideDialog}
                onSelect={changeSelectedList}
                initialListId={props.route.params.listId}
            />
        </>
    );
};

export default ListMainTabs;
