import React from 'react';
import {StyleSheet, StatusBar} from 'react-native';
import {useTheme, Appbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import {
    createDrawerNavigator,
    DrawerItemList,
    DrawerContentScrollView,
    DrawerContentComponentProps,
} from '@react-navigation/drawer';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {getHeaderTitle} from '@react-navigation/elements';

import RandomNumber from 'screens/RandomNumber';
import RandomColor from 'screens/RandomColor';
import RandomLetter from 'screens/RandomLetter';
import RandomPassword from 'screens/RandomPassword';
import DiceRoll from 'screens/DiceRoll';
import CoinFlip from 'screens/CoinFlip';
import RandomCountry from 'screens/RandomCountry';
import RandomChemicalElement from 'screens/RandomChemicalElement';
import DateTime from 'navigation/DateTimeNavigator';

import SelectList from 'screens/SelectList';
import CreateList from 'screens/CreateList';
import PickFromMultipleListsModal from 'screens/PickFromMultipleListsModal';
import ListMainTabs from 'navigation/ListMainTabs';

import Settings from 'screens/Settings';
import AboutScreen from 'screens/About';
import LicenseInfo from 'screens/LicenseInfo';
import Translators from 'screens/Translators';

import AppLogo from 'components/AppLogo';
import DrawerAppbar from 'components/DrawerAppbar';

import {useLocalization} from 'context/LocalizationContext';
import {usePreferences} from 'context/PreferencesContext';

import {DrawerParamList, MainStackParamList} from './types';

const Drawer = createDrawerNavigator<DrawerParamList>();

const CustomDrawerComponent = (props: DrawerContentComponentProps) => (
    <DrawerContentScrollView>
        <AppLogo />
        <DrawerItemList {...props} />
    </DrawerContentScrollView>
);

const drawerIcon =
    (icon: string) =>
    ({color, size}: {color: string; size: number}) =>
        <Icon name={icon} color={color} size={size} />;

const DrawerNavigator = () => {
    const theme = useTheme();
    const {translate} = useLocalization();
    const {initialScreen} = usePreferences();

    return (
        <Drawer.Navigator
            drawerContent={CustomDrawerComponent}
            screenOptions={{
                // drawerActiveTintColor: theme.dark ? '#4fb8b8' : '#005a5b',
                drawerActiveTintColor: theme.colors.primary,
                header: ({route, options}) => (
                    <DrawerAppbar
                        title={getHeaderTitle(options, route.name)}
                        style={[styles.appbar, options.headerStyle]}
                        // @ts-expect-error
                        {...options.headerProps}
                    />
                ),
            }}
            backBehavior="initialRoute"
            initialRouteName={initialScreen}>
            <Drawer.Screen
                name="Number"
                component={RandomNumber}
                options={{
                    title: translate('number'),
                    drawerIcon: drawerIcon('numeric'),
                }}
            />
            <Drawer.Screen
                name="CoinFlip"
                component={CoinFlip}
                options={{
                    title: translate('coinFlip'),
                    drawerIcon: drawerIcon('hand-coin'),
                }}
            />
            <Drawer.Screen
                name="List"
                component={SelectList}
                options={{
                    title: translate('list'),
                    drawerIcon: drawerIcon('format-list-bulleted'),
                }}
            />
            <Drawer.Screen
                name="Letter"
                component={RandomLetter}
                options={{
                    title: translate('letter'),
                    drawerIcon: drawerIcon('alphabetical'),
                }}
            />
            <Drawer.Screen
                name="Color"
                component={RandomColor}
                options={{
                    title: translate('color'),
                    drawerIcon: drawerIcon('palette'),
                }}
            />
            <Drawer.Screen
                name="Password"
                component={RandomPassword}
                options={{
                    title: translate('password'),
                    drawerIcon: drawerIcon('shield-account'),
                }}
            />
            <Drawer.Screen
                name="DiceRoll"
                component={DiceRoll}
                options={{
                    title: translate('diceRoll'),
                    drawerIcon: drawerIcon('dice-multiple'),
                }}
            />
            <Drawer.Screen
                name="Country"
                component={RandomCountry}
                options={{
                    title: translate('country'),
                    drawerIcon: drawerIcon('earth'),
                }}
            />
            <Drawer.Screen
                name="DateTime"
                component={DateTime}
                options={{
                    title: translate('dateTime'),
                    drawerIcon: drawerIcon('calendar'),
                    headerStyle: {
                        margin: 0,
                        marginTop: StatusBar.currentHeight,
                        borderRadius: 0,
                        backgroundColor: theme.colors.elevation.level1,
                    },
                    // @ts-expect-error
                    headerProps: {
                        elevated: false,
                    },
                }}
            />
            <Drawer.Screen
                name="ChemicalElement"
                component={RandomChemicalElement}
                options={{
                    title: translate('chemicalElement'),
                    drawerIcon: drawerIcon('flask'),
                }}
            />
        </Drawer.Navigator>
    );
};

const Stack = createNativeStackNavigator<MainStackParamList>();

const MainNavigator = () => {
    const {translate} = useLocalization();
    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                animation: 'fade_from_bottom',
            }}>
            <Stack.Screen name="Home" component={DrawerNavigator} />

            <Stack.Screen name="ListMain" component={ListMainTabs} />

            <Stack.Screen name="CreateList" component={CreateList} />

            <Stack.Group
                screenOptions={{
                    headerShown: true,
                    header: ({navigation, options, route}) => (
                        <Appbar.Header elevated>
                            <Appbar.BackAction onPress={navigation.goBack} />
                            <Appbar.Content
                                title={getHeaderTitle(options, route.name)}
                            />
                        </Appbar.Header>
                    ),
                }}>
                <Stack.Screen
                    name="About"
                    component={AboutScreen}
                    options={{
                        title: translate('about'),
                    }}
                />
                <Stack.Screen
                    name="Settings"
                    component={Settings}
                    options={{
                        title: translate('settings'),
                    }}
                />
                <Stack.Screen
                    name="Translators"
                    component={Translators}
                    options={{
                        title: translate('translations'),
                    }}
                />
                <Stack.Screen
                    name="LicenseInfo"
                    component={LicenseInfo}
                    options={{
                        title: translate('licenseInformation'),
                    }}
                />
            </Stack.Group>

            <Stack.Screen
                name="PickFromMultipleLists"
                component={PickFromMultipleListsModal}
                options={{
                    presentation: 'modal',
                }}
            />
        </Stack.Navigator>
    );
};

const styles = StyleSheet.create({
    appbar: {
        margin: 8,
        marginTop: (StatusBar.currentHeight ?? 0) + 8,
        borderRadius: 8,
    },
});

export default MainNavigator;
