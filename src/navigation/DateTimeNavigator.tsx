import React from 'react';
import {useTheme} from 'react-native-paper';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

import RandomDate from 'screens/RandomDate';
import RandomTime from 'screens/RandomTime';

import {useLocalization} from 'context/LocalizationContext';
import {DateTimeParamList} from './types';

const Tab = createMaterialTopTabNavigator<DateTimeParamList>();

const TabNavigator = () => {
    const theme = useTheme();
    const {translate} = useLocalization();

    return (
        <Tab.Navigator
            tabBarPosition="top"
            screenOptions={{
                tabBarStyle: {
                    backgroundColor: theme.colors.elevation.level1,
                },
                tabBarIndicatorStyle: {
                    backgroundColor: theme.colors.secondary,
                },
                tabBarActiveTintColor: theme.colors.onSurface,
                tabBarInactiveTintColor: theme.colors.onSurfaceVariant,
            }}>
            <Tab.Screen
                name="Date"
                component={RandomDate}
                options={{
                    tabBarLabel: translate('date'),
                }}
            />
            <Tab.Screen
                name="Time"
                component={RandomTime}
                options={{
                    tabBarLabel: translate('time'),
                }}
            />
        </Tab.Navigator>
    );
};

export default TabNavigator;
