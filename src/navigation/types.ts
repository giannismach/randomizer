import {ListID} from 'utils/types';
import {CompositeNavigationProp} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import {MaterialTopTabNavigationProp} from '@react-navigation/material-top-tabs';

export type DrawerParamList = {
    Number: {};
    CoinFlip: {};
    List: {};
    Letter: {};
    Color: {};
    Password: {};
    DiceRoll: {};
    Country: {};
    DateTime: {};
    ChemicalElement: {};
};

export type DrawerScreenRouteName = keyof DrawerParamList;

export type DateTimeParamList = {
    Date: {};
    Time: {};
};

export type ListMainTabsParamList = {
    Element: {};
    Shuffle: {};
    Groups: {};
    Edit: {};
};

export type ListMainTabsNavigationProp<T extends keyof ListMainTabsParamList> =
    CompositeNavigationProp<
        MaterialTopTabNavigationProp<ListMainTabsParamList, T>,
        NativeStackNavigationProp<MainStackParamList, 'ListMain'>
    >;

export type MainStackParamList = {
    Home: undefined;
    ListMain: {
        listId: ListID;
    };
    CreateList: undefined;
    PickFromMultipleLists: {};
    About: undefined;
    Settings: undefined;
    Translators: undefined;
    LicenseInfo: undefined;
};

export type DrawerScreenNavigationProp<T extends DrawerScreenRouteName = any> =
    CompositeNavigationProp<
        DrawerNavigationProp<DrawerParamList, T>,
        NativeStackNavigationProp<MainStackParamList, 'Home'>
    >;
