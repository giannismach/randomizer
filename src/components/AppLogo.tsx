import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
import {Text} from 'react-native-paper';
import globalStyles from 'utils/globalStyles';

const AppLogo = () => {
    return (
        <View style={[styles.headerContainer, globalStyles.centerWithoutFlex]}>
            <Image
                source={require('../assets/images/icon.webp')}
                style={styles.icon}
            />
            <Text variant="titleLarge" style={styles.title}>
                Randomizer
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    headerContainer: {
        paddingVertical: 24,
    },
    icon: {
        height: 55,
        width: 55,
    },
    title: {
        paddingTop: 16,
    },
});

export default React.memo(AppLogo);
