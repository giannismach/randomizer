import React from 'react';
import {
    Dialog,
    TextInput,
    Portal,
    Button,
    useTheme,
    HelperText,
} from 'react-native-paper';
import {useLocalization} from 'context/LocalizationContext';
import globalStyles from 'utils/globalStyles';

const EditDialog = (props: {
    visible: boolean;
    onDismiss: () => void;
    title: string;
    inputLabel?: string;
    initialValue: string;
    validate?: (value: string) => string | true; // string: error message, true: passed validation
    onSubmit: (value: string) => void;
}) => {
    const {colors} = useTheme();
    const {translate} = useLocalization();

    const [value, setValue] = React.useState(props.initialValue);
    const [errorMessage, setErrorMessage] = React.useState<string | null>(null);

    React.useEffect(() => {
        setErrorMessage(null);
    }, [value]);

    const dismiss = React.useCallback(() => {
        setErrorMessage(null);
        setValue(props.initialValue);
        props.onDismiss();
    }, [props]);

    const submit = React.useCallback(() => {
        const validationResult = props.validate?.(value) ?? true;
        if (validationResult === true) {
            props.onSubmit(value.trim());
            props.onDismiss();
        } else {
            setErrorMessage(validationResult);
        }
    }, [value, props]);

    return (
        <Portal>
            <Dialog
                visible={props.visible}
                onDismiss={dismiss}
                dismissable={
                    value === props.initialValue || value.trim() === ''
                }>
                <Dialog.Title>{props.title}</Dialog.Title>
                <Dialog.Content>
                    <TextInput
                        defaultValue={props.initialValue}
                        onChangeText={setValue}
                        label={props.inputLabel}
                        error={errorMessage !== null}
                        onSubmitEditing={submit}
                        autoFocus
                    />
                    <HelperText type="error" visible={errorMessage !== null}>
                        {errorMessage}
                    </HelperText>
                </Dialog.Content>
                <Dialog.Actions>
                    <Button
                        mode="text"
                        uppercase={false}
                        textColor={colors.secondary}
                        onPress={dismiss}
                        style={globalStyles.dialogAction}>
                        {translate('cancel')}
                    </Button>
                    <Button
                        mode="text"
                        uppercase={false}
                        textColor={colors.secondary}
                        onPress={submit}
                        style={globalStyles.dialogAction}
                        disabled={value.trim() === '' || errorMessage !== null}>
                        OK
                    </Button>
                </Dialog.Actions>
            </Dialog>
        </Portal>
    );
};

export default EditDialog;
