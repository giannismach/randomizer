import React from 'react';
import {StyleSheet, View} from 'react-native';
import {TextInput, Button} from 'react-native-paper';
import {useLocalization} from 'context/LocalizationContext';
import globalStyles from 'utils/globalStyles';

const ListItemInput = React.forwardRef(
    ({submit}: {submit(item: string): void}, ref) => {
        const {translate} = useLocalization();

        const [listItem, setListItem] = React.useState('');

        const reset = React.useCallback(() => {
            setListItem('');
        }, []);

        const onSubmit = React.useCallback(() => {
            submit(listItem);
            setListItem('');
        }, [listItem, submit]);

        React.useImperativeHandle(ref, () => ({reset}), [reset]);

        return (
            <View
                style={[
                    styles.itemInputContainer,
                    globalStyles.inlineContainer,
                ]}>
                <TextInput
                    value={listItem}
                    onChangeText={setListItem}
                    onSubmitEditing={onSubmit}
                    style={styles.itemInput}
                    mode="outlined"
                    disableFullscreenUI
                    accessible
                    label={translate('newItem')}
                />
                <Button
                    mode="contained"
                    disabled={listItem.trim() === ''}
                    uppercase={false}
                    icon="plus"
                    style={styles.primaryButton}
                    onPress={onSubmit}>
                    {translate('add')}
                </Button>
            </View>
        );
    }
);

const styles = StyleSheet.create({
    itemInputContainer: {
        padding: 16,
        paddingTop: 8,
    },
    itemInput: {
        width: '55%',
    },
    primaryButton: {
        marginLeft: 16,
        flex: 1,
    },
});

export default ListItemInput;
