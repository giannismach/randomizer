import React, {useCallback} from 'react';
import {FlatList} from 'react-native';
import {useListsContext} from 'context/ListsContext';
import {Checkbox} from 'react-native-paper';
import {ListWithItems} from 'utils/types';

const ListMultipleSelection = ({
    selectedLists,
    setSelectedLists,
}: {
    selectedLists: number[];
    setSelectedLists: React.Dispatch<React.SetStateAction<number[]>>;
}) => {
    const {data} = useListsContext();

    const toggleList = useCallback(
        (id: number) => {
            setSelectedLists(lists => {
                if (lists.includes(id)) {
                    const ind = lists.indexOf(id);
                    if (ind === -1) {
                        return lists;
                    }
                    return lists.slice(0, ind).concat(lists.slice(ind + 1));
                } else {
                    return [...lists, id];
                }
            });
        },
        [setSelectedLists]
    );

    const renderItem = ({item: {list}}: {item: ListWithItems}) => (
        <Checkbox.Item
            status={selectedLists.includes(list.id) ? 'checked' : 'unchecked'}
            label={list.name}
            onPress={() => toggleList(list.id)}
        />
    );

    const keyExtractor = ({list}: ListWithItems) => list.id.toString();

    return (
        <FlatList
            data={data}
            renderItem={renderItem}
            keyExtractor={keyExtractor}
        />
    );
};

export default ListMultipleSelection;
