import React, {useMemo} from 'react';
import {View, StyleSheet} from 'react-native';
import {useTheme, Text} from 'react-native-paper';
import Slider from '@react-native-community/slider';

/**
 * Based on the Slider component from @react-native-community/slider.
 * Includes theming support, limit captions, positioning-layout.
 */
const CustomSlider = (
    props: React.ComponentPropsWithoutRef<typeof Slider> & {
        hideDetails?: boolean;
    }
) => {
    const theme = useTheme();

    const sliderProps: React.ComponentPropsWithoutRef<typeof Slider> = useMemo(
        () => ({
            ...props,
            maximumTrackTintColor: theme.colors.onBackground,
            minimumTrackTintColor: theme.colors.secondary,
            thumbTintColor: theme.colors.secondary,
            accessible: true,
            accessibilityRole: 'adjustable',
        }),
        [props, theme.colors.secondary, theme.colors.onBackground]
    );

    if (props.hideDetails) {
        return <Slider {...sliderProps} />;
    }

    return (
        <View style={styles.sliderContainer}>
            <Text variant="bodySmall" style={styles.sliderText}>
                {props.minimumValue}
            </Text>
            <Slider {...sliderProps} />
            <Text variant="bodySmall" style={styles.sliderText}>
                {props.maximumValue}
            </Text>
        </View>
    );
};

const styles = StyleSheet.create({
    sliderContainer: {
        paddingBottom: 8,
        justifyContent: 'center',
        flexDirection: 'row',
    },
    sliderText: {
        fontSize: 16,
    },
});

export default CustomSlider;
