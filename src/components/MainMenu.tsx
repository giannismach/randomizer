import React, {useCallback} from 'react';
import {StatusBar, Linking} from 'react-native';
import {Menu} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import {useLocalization} from 'context/LocalizationContext';
import useBooleanState from 'hooks/useBooleanState';
import {DrawerScreenNavigationProp} from 'navigation/types';

interface MenuProps {
    anchor: ({onPress}: {onPress: () => void}) => React.ReactNode;
}

const MainMenu = (props: MenuProps) => {
    const navigation = useNavigation<DrawerScreenNavigationProp>();
    const {translate} = useLocalization();

    const [menuVisible, openMenu, closeMenu] = useBooleanState();

    const goToSettings = useCallback(() => {
        navigation.navigate('Settings');
        closeMenu();
    }, [closeMenu, navigation]);

    const goToPlayStore = useCallback(() => {
        closeMenu();
        const appURL = 'market://details?id=com.giannis.randomizer';
        const browserURL =
            'https://play.google.com/store/apps/details?id=com.giannis.randomizer';
        Linking.canOpenURL(appURL).then(supported => {
            return Linking.openURL(supported ? appURL : browserURL);
        });
    }, [closeMenu]);

    return (
        <Menu
            visible={menuVisible}
            style={{
                marginTop: StatusBar.currentHeight,
            }}
            onDismiss={closeMenu}
            anchor={props.anchor({onPress: openMenu})}>
            <Menu.Item
                title={translate('settings')}
                onPress={goToSettings}
                leadingIcon="cog"
            />
            <Menu.Item
                title={translate('rateOnGooglePlay')}
                onPress={goToPlayStore}
                leadingIcon="star"
            />
        </Menu>
    );
};

export default React.memo(MainMenu);
