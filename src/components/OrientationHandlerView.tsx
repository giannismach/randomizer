import React from 'react';
import {View, ViewProps} from 'react-native';
import useScreenOrientation from 'hooks/useScreenOrientation';
import globalStyles from 'utils/globalStyles';

const OrientationHandlerView = (props: ViewProps) => {
    const orientation = useScreenOrientation();
    return (
        <View
            {...props}
            style={[
                props.style,
                globalStyles.flex,
                orientation === 'landscape'
                    ? globalStyles.row
                    : globalStyles.column,
            ]}
        />
    );
};

export default OrientationHandlerView;
