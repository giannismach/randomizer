import React from 'react';
import {StyleSheet} from 'react-native';
import {FAB} from 'react-native-paper';

type FABProps = React.ComponentPropsWithoutRef<typeof FAB>;

const MainFAB = (props: FABProps) => {
    return (
        <FAB
            uppercase
            accessible
            accessibilityRole="button"
            accessibilityLabel={props.label}
            onAccessibilityTap={props.onPress}
            {...props}
            style={[styles.mainFAB, props.style]}
        />
    );
};

const styles = StyleSheet.create({
    mainFAB: {
        position: 'absolute',
        right: 16,
        bottom: 24,
        zIndex: 1,
    },
});

export default React.memo(MainFAB);
