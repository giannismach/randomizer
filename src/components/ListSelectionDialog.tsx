import React, {useCallback} from 'react';
import {FlatList, StyleSheet, View} from 'react-native';
import {
    Dialog,
    RadioButton,
    Portal,
    Text,
    List,
    useTheme,
} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import {useListsContext} from 'context/ListsContext';
import {useLocalization} from 'context/LocalizationContext';
import {ListWithItems} from 'utils/types';

interface ListSelectionDialogProps {
    visible: boolean;
    onDismiss: () => void;
    onSelect: (list: ListWithItems) => void;
    initialListId?: number;
}

const ListSelectionDialog = (props: ListSelectionDialogProps) => {
    const {colors} = useTheme();
    const {data} = useListsContext();
    const {translate} = useLocalization();

    const keyExtractor = useCallback(
        ({list: {id}}: ListWithItems) => id.toString(),
        []
    );
    const renderItem = useCallback(
        ({item}: {item: ListWithItems}) => {
            const numberOfElements = item.items.length;
            return (
                <List.Item
                    title={item.list.name}
                    description={`${numberOfElements} ${translate(
                        numberOfElements === 1 ? 'element' : 'elements'
                    )}`}
                    onPress={() => props.onSelect(item)}
                    left={({style}) => (
                        <View style={style}>
                            <RadioButton
                                value={item.list.id.toString()}
                                onPress={() => props.onSelect(item)}
                                status={
                                    props.initialListId === item.list.id
                                        ? 'checked'
                                        : 'unchecked'
                                }
                                color={colors.inversePrimary}
                                uncheckedColor={colors.outline}
                            />
                        </View>
                    )}
                />
            );
        },
        [props, translate]
    );

    return (
        <Portal>
            <Dialog
                visible={props.visible}
                onDismiss={props.onDismiss}
                style={styles.dialog}>
                <Dialog.Title>{translate('selectList')}</Dialog.Title>
                <Dialog.ScrollArea>
                    <FlatList
                        data={data}
                        ListEmptyComponent={
                            <Text style={styles.noListsFoundText}>
                                {translate('noListsFound')}
                            </Text>
                        }
                        renderItem={renderItem}
                        keyExtractor={keyExtractor}
                    />
                </Dialog.ScrollArea>
            </Dialog>
        </Portal>
    );
};

const styles = StyleSheet.create({
    dialog: {
        maxHeight: moderateScale(600),
    },
    noListsFoundText: {
        textAlign: 'center',
        margin: moderateScale(10),
        fontSize: moderateScale(16),
        padding: moderateScale(5),
    },
});

export default React.memo(ListSelectionDialog);
