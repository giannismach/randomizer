import React, {useCallback} from 'react';
import {Keyboard} from 'react-native';
import {Appbar} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import {DrawerNavigationProp} from '@react-navigation/drawer';
import MainMenu from './MainMenu';

type DrawerAppbarProps = React.ComponentProps<typeof Appbar.Header> & {
    title: string;
    subtitle?: string;
};

/**
 * Based on the Appbar component from react-native-paper.
 * Additional features: drawer management, actions menu.
 */
const DrawerAppbar = (props: DrawerAppbarProps) => {
    const navigation = useNavigation<DrawerNavigationProp<{}>>();

    const openDrawer = useCallback(() => {
        Keyboard.dismiss();
        navigation.toggleDrawer();
    }, [navigation]);

    const menuAnchor = useCallback(
        // @ts-ignore
        p => (
            <Appbar.Action
                icon="dots-vertical"
                accessibilityLabel="Open actions menu"
                {...p}
            />
        ),
        []
    );

    return (
        <Appbar style={props.style} elevated mode="center-aligned" {...props}>
            <Appbar.Action
                icon="menu"
                isLeading
                onPress={openDrawer}
                accessibilityLabel="Open drawer menu"
            />
            <Appbar.Content title={props.title} />
            <MainMenu anchor={menuAnchor} />
        </Appbar>
    );
};

export default React.memo(DrawerAppbar);
