import React from 'react';
import {StyleSheet} from 'react-native';
import {useTheme} from 'react-native-paper';
import {Picker} from '@react-native-picker/picker';
import {PickerProps} from '@react-native-picker/picker/typings/Picker';

/**
 * Based on the Picker component from @react-native-picker/picker.
 * Additionally includes theming support.
 */
const CustomPicker = (
    props: PickerProps & {width: number; children: React.ReactElement[]}
) => {
    const theme = useTheme();

    return (
        <Picker
            mode="dropdown"
            {...props}
            style={[
                {
                    width: props.width,
                    color: theme.colors.onBackground,
                },
                props.style,
                styles.picker,
            ]}
            dropdownIconColor={theme.colors.onBackground}
        />
    );
};

CustomPicker.Item = Picker.Item;

const styles = StyleSheet.create({
    picker: {
        height: 50,
    },
});

export default CustomPicker;
