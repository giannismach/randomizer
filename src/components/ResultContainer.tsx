import React, {useCallback, useMemo, useImperativeHandle} from 'react';
import {StyleSheet, ToastAndroid} from 'react-native';
import {Surface, IconButton, useTheme} from 'react-native-paper';
import * as Animatable from 'react-native-animatable';
import {moderateScale} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import MainFAB from './MainFAB';
import {useLocalization} from 'context/LocalizationContext';
import useRandomResult from 'hooks/useRandomResult';
import useScreenOrientation from 'hooks/useScreenOrientation';
import Clipboard from 'modules/Clipboard';
import globalStyles from 'utils/globalStyles';

type ResultDetails = {
    [key: string]: any;
};
type ResultDetailsDefinition<T, D extends ResultDetails> = {
    [P in keyof D]: (res: T) => D[P];
};

interface ResultContainerProps<T, D extends ResultDetails> {
    result: {
        initialValue: T;
        generate: () => T;
        // display: (res: T, details: D) => React.ReactNode;

        beforeUpdate?: () => boolean;
        afterUpdate?: () => void;

        isEmpty: (res: T) => boolean;
        processForClipboard?: (res: T) => string;
        details?: ResultDetailsDefinition<T, D>;

        ref?: React.Ref<any>;
    };

    children: (result: T, details: D) => React.ReactNode;

    animation: (ref: React.Ref<any>) => void;

    noSurface?: boolean;
    noPadding?: boolean;
    noCenter?: boolean;

    showClipboardButton?: boolean;

    fab?: React.ComponentPropsWithoutRef<typeof MainFAB>;

    emptyIcon: string;
    emptyIconSize?: number;
}

const ResultContainer = <T, D extends ResultDetails = {}>(
    props: ResultContainerProps<T, D>
) => {
    const {colors} = useTheme();
    const {translate} = useLocalization();

    const orientation = useScreenOrientation();

    const {
        ref: outputRef,
        result,
        update,
        reset,
    } = useRandomResult({
        initialValue: props.result.initialValue,
        generateResult: props.result.generate,
        beforeUpdate: props.result.beforeUpdate,
        afterUpdate: props.result.afterUpdate,
        animation: props.animation,
    });

    useImperativeHandle(props.result.ref, () => ({reset}), [reset]);

    const copyString = useCallback(() => {
        if (props.result.processForClipboard) {
            Clipboard.setString(props.result.processForClipboard(result));
        }
        ToastAndroid.show(translate('copiedToClipboard'), ToastAndroid.SHORT);
    }, [props.result, result, translate]);

    const details = useMemo(
        () =>
            props.result.details && !props.result.isEmpty(result)
                ? Object.keys(props.result.details).reduce(
                      (acc, cur: keyof D) => ({
                          ...acc,
                          [cur]: props.result.details?.[cur](result),
                      }),
                      {} as D
                  )
                : ({} as D),
        [props.result, result]
    );

    const mainContent = (
        <>
            {!props.result.isEmpty(result) &&
                props.result.processForClipboard &&
                props.showClipboardButton && (
                    <IconButton
                        icon="content-copy"
                        style={styles.clipboardButton}
                        onPress={copyString}
                        accessibilityLabel="Copy result"
                    />
                )}
            <Animatable.View
                ref={outputRef}
                useNativeDriver
                accessibilityLiveRegion="polite"
                style={[
                    props.noCenter && !props.result.isEmpty(result)
                        ? globalStyles.flex
                        : globalStyles.centerWithFlex,
                ]}>
                {props.result.isEmpty(result) ? (
                    <Icon
                        name={props.emptyIcon}
                        size={moderateScale(props.emptyIconSize ?? 60)}
                        color={colors.onSurfaceDisabled}
                    />
                ) : (
                    props.children(result, details)
                )}
            </Animatable.View>
            {props.fab && (
                <MainFAB
                    {...props.fab}
                    label={
                        orientation === 'portrait' ? props.fab.label : undefined
                    }
                    onPress={update}
                />
            )}
        </>
    );

    if (props.noSurface) {
        return mainContent;
    }

    return (
        <Surface
            elevation={1}
            style={[
                styles.resultContainer,
                props.noPadding && !props.result.isEmpty(result)
                    ? {}
                    : styles.resultContainerPadding,
            ]}>
            {mainContent}
        </Surface>
    );
};

const styles = StyleSheet.create({
    resultContainer: {
        margin: 8,
        flex: 1,
        borderRadius: 4,
    },
    resultContainerPadding: {
        paddingBottom: 25,
    },
    clipboardButton: {
        position: 'absolute',
        zIndex: 2,
        right: 2,
        top: 2,
    },
});

export default ResultContainer;
