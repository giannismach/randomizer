import React, {useCallback} from 'react';
import {Button, useTheme} from 'react-native-paper';
import {useNetInfo} from '@react-native-community/netinfo';
import WebBrowserUtils from 'utils/WebBrowserUtils';

type ButtonProps = Omit<
    React.ComponentPropsWithoutRef<typeof Button>,
    'children'
>;

interface AnchorProps {
    href: string;
    icon: string;
    text: string;
}

const Anchor = (props: AnchorProps & ButtonProps) => {
    const theme = useTheme();
    const {isConnected} = useNetInfo();

    WebBrowserUtils.useWarmUp();
    WebBrowserUtils.useMayInitWithUrl(props.href);

    const onPress = useCallback(() => {
        WebBrowserUtils.openBrowserWithFallback(props.href, {
            toolbarColor: theme.colors.primary,
            enableBarCollapsing: false,
            enableDefaultShareMenuItem: true,
            showTitle: true,
            showInRecents: false,
            createTask: false,
        });
    }, [props.href, theme.colors]);

    return (
        <Button
            mode="text"
            onPress={onPress}
            color={theme.colors.secondary}
            uppercase={false}
            accessible
            accessibilityLabel={props.text}
            disabled={!isConnected}
            {...props}>
            {props.text}
        </Button>
    );
};

export default React.memo(Anchor);
