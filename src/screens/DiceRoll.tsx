import React from 'react';
import {StyleSheet, View} from 'react-native';
import {List, Text, useTheme} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import OrientationHandlerView from 'components/OrientationHandlerView';
import ResultContainer from 'components/ResultContainer';
import Slider from 'components/Slider';
import {useLocalization} from 'context/LocalizationContext';
import useOption from 'hooks/useOption';
import useScreenOrientation from 'hooks/useScreenOrientation';
import globalStyles from 'utils/globalStyles';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';
import Converters from 'utils/Converters';

const DiceRoll = () => {
    const theme = useTheme();
    const {translate} = useLocalization();

    const orientation = useScreenOrientation();

    const [quantity, setQuantity, initialSliderValue] = useOption({
        defaultValue: 1,
        asyncStorageKey: '@Randomizer_DiceRoll_quantity',
        converter: Converters.stringToNumber,
    });

    return (
        <OrientationHandlerView>
            <View style={orientation === 'landscape' && {minWidth: '40%'}}>
                <List.Item title={`${translate('numberOfDice')} ${quantity}`} />
                <Slider
                    value={initialSliderValue}
                    onValueChange={setQuantity}
                    minimumValue={1}
                    maximumValue={10}
                    step={1}
                    style={styles.slider}
                    accessibilityHint="Number of dice"
                />
            </View>
            <ResultContainer<
                number[],
                {
                    sum: number;
                    average: string;
                }
            >
                animation={animations.fadeIn}
                result={{
                    initialValue: [],
                    details: {
                        sum: res => res.reduce((acc, cur) => acc + cur, 0),
                        average: res =>
                            res.length > 0
                                ? (
                                      res.reduce((acc, cur) => acc + cur, 0) /
                                      res.length
                                  ).toFixed(2)
                                : '0',
                    },
                    generate: () => Random.integers(1, 6, quantity),
                    isEmpty: res => res.length === 0,
                }}
                fab={{
                    icon: 'dice-multiple',
                    label: translate('rollTheDice'),
                }}
                emptyIcon="dice-multiple"
                emptyIconSize={70}>
                {(result, details) => (
                    <>
                        <View
                            style={[
                                globalStyles.centerWithoutFlex,
                                globalStyles.inlineContainer,
                                styles.resultView,
                            ]}>
                            {result.length !== 0 &&
                                result.map(dice => (
                                    <Icon
                                        name={`dice-${dice}`}
                                        size={moderateScale(
                                            result.length <= 5 ? 65 : 50
                                        )}
                                        color={theme.colors.onSurface}
                                    />
                                ))}
                        </View>
                        {result.length > 1 && (
                            <>
                                <Text
                                    variant="bodySmall"
                                    style={styles.secondaryOutputText}>
                                    {translate('sum')} {details.sum}
                                </Text>
                                <Text
                                    variant="bodySmall"
                                    style={styles.secondaryOutputText}>
                                    {translate('average')} {details.average}
                                </Text>
                            </>
                        )}
                    </>
                )}
            </ResultContainer>
        </OrientationHandlerView>
    );
};

const styles = StyleSheet.create({
    slider: {
        width: moderateScale(200),
    },
    resultView: {
        padding: 4,
    },
    secondaryOutputText: {
        textAlign: 'center',
        fontSize: moderateScale(12),
    },
});

export default DiceRoll;
