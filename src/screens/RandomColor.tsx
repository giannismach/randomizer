import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Text, SegmentedButtons} from 'react-native-paper';
import {scale, moderateScale} from 'react-native-size-matters';
import ResultContainer from 'components/ResultContainer';
import {useLocalization} from 'context/LocalizationContext';
import useOption from 'hooks/useOption';
import Random from 'utils/Random';
import Validators from 'utils/Validators';
import {animations} from 'utils/Utils';

type ColorType = 'hex' | 'rgb' | 'hsl';

const RandomColor = () => {
    const {translate} = useLocalization();

    const [colorType, setColorType] = useOption<ColorType>({
        defaultValue: 'hex',
        asyncStorageKey: '@Randomizer_RandomColor_colorType',
        validator: Validators.colorType,
    });

    return (
        <>
            <View style={styles.segmentedButtonsContainer}>
                <SegmentedButtons
                    value={colorType}
                    onValueChange={val => {
                        setColorType(val as ColorType);
                    }}
                    buttons={[
                        {
                            value: 'hex',
                            label: translate('hexadecimal'),
                        },
                        {
                            value: 'rgb',
                            label: 'RGB',
                        },
                        {
                            value: 'hsl',
                            label: 'HSL',
                        },
                    ]}
                />
            </View>
            <ResultContainer<string>
                animation={animations.fadeIn}
                showClipboardButton
                result={{
                    initialValue: '',
                    generate: () => {
                        if (colorType === 'rgb') {
                            return Random.colors.RGB();
                        } else if (colorType === 'hex') {
                            return Random.colors.hexadecimal();
                        } else if (colorType === 'hsl') {
                            return Random.colors.HSL();
                        }
                        return '';
                    },
                    isEmpty: res => res === '',
                    processForClipboard: res => res,
                }}
                fab={{
                    icon: 'shuffle',
                    label: translate('generateColor'),
                }}
                emptyIcon="palette"
                emptyIconSize={70}>
                {res => (
                    <>
                        <Text
                            style={[
                                {
                                    backgroundColor: res,
                                },
                                styles.colorPreview,
                            ]}
                        />
                        <Text style={styles.colorName}>{res}</Text>
                    </>
                )}
            </ResultContainer>
        </>
    );
};

const styles = StyleSheet.create({
    segmentedButtonsContainer: {
        paddingHorizontal: 16,
        paddingVertical: 8,
    },
    colorName: {
        fontSize: moderateScale(20),
    },
    colorPreview: {
        borderColor: 'lightgrey',
        borderWidth: 1,
        borderRadius: 10,
        height: scale(60),
        width: scale(60),
        margin: moderateScale(15),
    },
});

export default RandomColor;
