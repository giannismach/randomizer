import React from 'react';
import {FlatList} from 'react-native';
import {List, Divider} from 'react-native-paper';
import ResultContainer from 'components/ResultContainer';
import {useLocalization} from 'context/LocalizationContext';
import {useSelectedList} from 'context/SelectedListContext';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';
import {ListItem} from 'utils/types';

const ShuffleList = () => {
    const {translate} = useLocalization();
    const {list, items} = useSelectedList();

    const ref = React.useRef<{reset: () => void}>({
        reset: () => {},
    });

    const keyExtractor = React.useCallback(
        ({id}: ListItem) => id.toString(),
        []
    );
    const renderItem = React.useCallback(
        ({item}: {item: ListItem}) => (
            <List.Item title={item.item} titleNumberOfLines={6} />
        ),
        []
    );

    React.useEffect(() => {
        ref.current.reset();
    }, [list.id]);

    return (
        <ResultContainer
            animation={animations.fadeIn}
            result={{
                initialValue: [],
                generate: () => Random.shuffle(items),
                isEmpty: res => res.length === 0,
                ref,
            }}
            fab={{
                icon: 'shuffle-variant',
                label: translate('shuffle'),
            }}
            noCenter
            noPadding
            emptyIcon="shuffle-variant"
            emptyIconSize={70}>
            {result => (
                <>
                    {result.length > 0 && (
                        <FlatList
                            data={result}
                            renderItem={renderItem}
                            keyExtractor={keyExtractor}
                            ItemSeparatorComponent={Divider}
                        />
                    )}
                </>
            )}
        </ResultContainer>
    );
};

export default React.memo(ShuffleList);
