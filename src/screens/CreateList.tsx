import React, {useState, useCallback, useReducer} from 'react';
import {
    FlatList,
    StyleSheet,
    Keyboard,
    ToastAndroid,
    StatusBar,
    View,
} from 'react-native';
import {
    Button,
    Appbar,
    TextInput,
    Divider,
    useTheme,
    List,
    IconButton,
} from 'react-native-paper';
import Snackbar from 'react-native-snackbar';
import {RouteProp} from '@react-navigation/native';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import ListItemInput from 'components/ListItemInput';
import ListSelectionDialog from 'components/ListSelectionDialog';
import OrientationHandlerView from 'components/OrientationHandlerView';
import {useListsContext} from 'context/ListsContext';
import {useLocalization} from 'context/LocalizationContext';
import useScreenOrientation from 'hooks/useScreenOrientation';
import {MainStackParamList} from 'navigation/types';
import reducer, {ReducerActionType} from 'utils/listItemReducer';
import globalStyles from 'utils/globalStyles';
import {ListWithItems, TemporaryListItem} from 'utils/types';
import useSorted from 'hooks/useSorted';

const CreateList = (props: {
    route: RouteProp<MainStackParamList, 'CreateList'>;
    navigation: NativeStackNavigationProp<MainStackParamList, 'CreateList'>;
}) => {
    const theme = useTheme();
    const {
        data: lists,
        actions: {create},
    } = useListsContext();
    const {translate} = useLocalization();
    const orientation = useScreenOrientation();

    const [listName, setListName] = useState('');

    const [[listItems], dispatch] = useReducer(reducer, [[], []]);

    const [dialogVisible, setDialogVisible] = useState(false);
    const showDialog = useCallback(() => {
        setDialogVisible(true);
        Keyboard.dismiss();
    }, []);
    const hideDialog = useCallback(() => setDialogVisible(false), []);

    const sortedListItems = useSorted(listItems, i => i.item);

    const addListItem = useCallback((item: string) => {
        dispatch({
            type: ReducerActionType.ADD,
            data: {
                item,
            },
        });
        Snackbar.dismiss();
    }, []);

    const removeListItem = useCallback((item: TemporaryListItem) => {
        dispatch({
            type: ReducerActionType.DELETE,
            data: {
                item,
            },
        });
        Snackbar.dismiss();
    }, []);

    const undoAppend = useCallback(() => {
        dispatch({
            type: ReducerActionType.UNDO_CHANGE,
        });
    }, []);

    const appendListItems = useCallback(
        (l: ListWithItems) => {
            hideDialog();
            dispatch({
                type: ReducerActionType.APPEND,
                data: {
                    list: l,
                },
            });
            Snackbar.show({
                text: translate('addedItemsFromList'),
                duration: Snackbar.LENGTH_LONG,
                backgroundColor: theme.colors.inverseSurface,
                action: {
                    text: translate('undo'),
                    textColor: theme.colors.inversePrimary,
                    onPress: undoAppend,
                },
            });
        },
        [hideDialog, theme.colors.secondary, translate, undoAppend]
    );

    const saveList = useCallback(() => {
        if (listName.trim() === '') {
            ToastAndroid.show(
                translate('blankListNameError'),
                ToastAndroid.SHORT
            );
        } else if (lists.some(({list: {name}}) => name === listName)) {
            ToastAndroid.show(
                translate('duplicateListNameError'),
                ToastAndroid.SHORT
            );
        } else if (listItems.length === 0) {
            ToastAndroid.show(
                translate('noBlankListError'),
                ToastAndroid.SHORT
            );
        } else {
            create(
                listName,
                listItems.map(({item}) => item),
                () => {
                    props.navigation.goBack();
                }
            );
        }
    }, [create, lists, listItems, listName, props.navigation, translate]);

    const itemKeyExtractor = React.useCallback(
        (item: TemporaryListItem) => item.key,
        []
    );
    const renderListItem = React.useCallback(
        ({item}: {item: TemporaryListItem}) => (
            <List.Item
                title={item.item}
                titleNumberOfLines={6}
                right={iconProps => (
                    <IconButton
                        icon="close"
                        onPress={() => removeListItem(item)}
                        accessibilityLabel="Delete item"
                        {...iconProps}
                    />
                )}
            />
        ),
        [removeListItem]
    );

    return (
        <>
            <Appbar.Header statusBarHeight={StatusBar.currentHeight} elevated>
                <Appbar.BackAction onPress={props.navigation.goBack} />
                <Appbar.Content title={translate('createList')} />
                <Appbar.Action
                    icon="check"
                    onPress={saveList}
                    disabled={listName.trim() === ''}
                    accessibilityLabel="Submit"
                />
            </Appbar.Header>
            <OrientationHandlerView>
                <View
                    style={[
                        globalStyles.column,
                        orientation === 'landscape'
                            ? globalStyles.flex
                            : globalStyles.noFlex,
                    ]}>
                    <TextInput
                        style={styles.listNameInput}
                        mode="outlined"
                        value={listName}
                        label={translate('listName')}
                        onChangeText={setListName}
                        maxLength={100}
                        disableFullscreenUI
                        accessible
                    />
                    <ListItemInput submit={addListItem} />
                    {lists.length > 0 && (
                        <>
                            <Button
                                mode="text"
                                icon="plus-box-multiple"
                                onPress={showDialog}
                                style={styles.secondaryButton}
                                color={theme.colors.secondary}
                                uppercase={false}>
                                {translate('addItemsFromOtherList')}
                            </Button>
                            {orientation === 'portrait' && <Divider />}
                        </>
                    )}
                </View>
                <View style={globalStyles.flex}>
                    <FlatList
                        data={sortedListItems}
                        renderItem={renderListItem}
                        keyExtractor={itemKeyExtractor}
                        ListHeaderComponent={
                            <List.Subheader>
                                {sortedListItems.length}{' '}
                                {translate(
                                    sortedListItems.length === 1
                                        ? 'element'
                                        : 'elements'
                                )}
                            </List.Subheader>
                        }
                    />
                </View>
            </OrientationHandlerView>
            <ListSelectionDialog
                visible={dialogVisible}
                onDismiss={hideDialog}
                onSelect={appendListItems}
            />
        </>
    );
};

export const styles = StyleSheet.create({
    listNameInput: {
        marginHorizontal: 16,
        marginVertical: 8,
    },
    itemInputContainer: {
        padding: 16,
        paddingTop: 8,
    },
    itemInput: {
        width: '55%',
    },
    primaryButton: {
        marginLeft: 16,
        flex: 1,
    },

    secondaryButton: {
        marginHorizontal: 16,
        marginTop: 4,
        marginBottom: 8,
    },
});

export default CreateList;
