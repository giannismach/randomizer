import React from 'react';
import {StyleSheet, View} from 'react-native';
import {Text} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import OrientationHandlerView from 'components/OrientationHandlerView';
import ResultContainer from 'components/ResultContainer';
import Picker from 'components/Picker';
import {useLocalization} from 'context/LocalizationContext';
import useOption from 'hooks/useOption';
import useScreenOrientation from 'hooks/useScreenOrientation';
import globalStyles from 'utils/globalStyles';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';

import alphabets from 'assets/strings/alphabets.json';

const validateAlphabet = (str: string) =>
    Object.keys(alphabets).indexOf(str) !== -1;

type LetterCase = 'both' | 'lower' | 'upper';
type Alphabet = keyof typeof alphabets;

const RandomLetter = () => {
    const {translate} = useLocalization();

    const orientation = useScreenOrientation();

    const [letterCase, setLetterCase] = useOption<LetterCase>({
        defaultValue: 'both',
        asyncStorageKey: '@Randomizer_RandomLetter_letterCase',
    });
    const [alphabet, setAlphabet] = useOption<Alphabet>({
        defaultValue: 'english',
        asyncStorageKey: '@Randomizer_RandomLetter_alphabet',
        validator: validateAlphabet,
    });

    return (
        <OrientationHandlerView>
            <View
                style={
                    orientation === 'landscape'
                        ? globalStyles.flex
                        : globalStyles.noFlex
                }>
                <View style={globalStyles.pickerContainer}>
                    <Text
                        variant="labelLarge"
                        style={globalStyles.pickerCaption}>
                        {translate('letterCase')}:
                    </Text>
                    <Picker
                        selectedValue={letterCase}
                        onValueChange={val => {
                            setLetterCase(val as LetterCase);
                        }}
                        width={200}>
                        <Picker.Item
                            label={translate('bothCases')}
                            value="both"
                        />
                        <Picker.Item
                            label={translate('lowercase')}
                            value="lower"
                        />
                        <Picker.Item
                            label={translate('uppercase')}
                            value="upper"
                        />
                    </Picker>
                </View>
                <View style={globalStyles.pickerContainer}>
                    <Text
                        variant="labelLarge"
                        style={globalStyles.pickerCaption}>
                        {translate('alphabet')}:
                    </Text>
                    <Picker
                        selectedValue={alphabet}
                        onValueChange={val => {
                            setAlphabet(val as Alphabet);
                        }}
                        width={180}>
                        {Object.keys(alphabets).map(alph => (
                            <Picker.Item
                                label={translate(alph as Alphabet)}
                                value={alph}
                                key={alph}
                            />
                        ))}
                    </Picker>
                </View>
            </View>
            <ResultContainer<string>
                animation={animations.fadeIn}
                result={{
                    initialValue: '',
                    generate: () => {
                        const {lower, upper} = alphabets[alphabet];
                        return Random.arrayElement(
                            letterCase === 'both'
                                ? [...lower, ...upper]
                                : letterCase === 'lower'
                                ? [...lower]
                                : [...upper]
                        );
                    },
                    isEmpty: res => res === '',
                }}
                fab={{
                    icon: 'shuffle',
                    label: translate('generateLetter'),
                }}
                emptyIcon="alphabetical"
                emptyIconSize={60}>
                {result => <Text style={styles.outputText}>{result}</Text>}
            </ResultContainer>
        </OrientationHandlerView>
    );
};

const styles = StyleSheet.create({
    outputText: {
        fontSize: moderateScale(50),
    },
});

export default RandomLetter;
