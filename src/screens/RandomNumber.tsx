import React, {useCallback} from 'react';
import {
    StyleSheet,
    Keyboard,
    ToastAndroid,
    View,
    ScrollView,
} from 'react-native';
import {Checkbox, List, Divider, Text, TextInput} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import OrientationHandlerView from 'components/OrientationHandlerView';
import ResultContainer from 'components/ResultContainer';
import Slider from 'components/Slider';
import {useLocalization} from 'context/LocalizationContext';
import useOption from 'hooks/useOption';
import useScreenOrientation from 'hooks/useScreenOrientation';
import Converters from 'utils/Converters';
import globalStyles from 'utils/globalStyles';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';

const RandomNumber = () => {
    const {translate} = useLocalization();

    const orientation = useScreenOrientation();

    const [rangeMin, setRangeMin] = useOption<string>({
        defaultValue: '0',
        asyncStorageKey: '@Randomizer_RandomNumber_rangeMin',
    });
    const [rangeMax, setRangeMax] = useOption<string>({
        defaultValue: '100',
        asyncStorageKey: '@Randomizer_RandomNumber_rangeMax',
    });
    const [quantity, setQuantity, initialSliderValue] = useOption({
        defaultValue: 1,
        asyncStorageKey: '@Randomizer_RandomNumber_quantity',
        converter: Converters.stringToNumber,
    });
    const [repetitionsAllowed, setRepetitionsAllowed] = useOption({
        defaultValue: true,
        asyncStorageKey: '@Randomizer_RandomNumber_repetitions',
        converter: Converters.stringToBoolean,
    });

    const toggleRepetitions = () => setRepetitionsAllowed(b => !b);

    const beforeUpdate = useCallback(() => {
        Keyboard.dismiss();
        if (
            [rangeMin, rangeMax].map(Number).some(Number.isNaN) ||
            [rangeMin, rangeMax].some(v => v === '')
        ) {
            ToastAndroid.show(
                translate('invalidNumbersError'),
                ToastAndroid.SHORT
            );
            return false;
        } else if (Number(rangeMin) >= Number(rangeMax)) {
            ToastAndroid.show(
                translate('maximumGreaterThanMinimumError'),
                ToastAndroid.SHORT
            );
            return false;
        } else if (
            !repetitionsAllowed &&
            Number(rangeMax) - Number(rangeMin) + 1 < quantity
        ) {
            ToastAndroid.show(
                translate('tooManyUniqueNumbersError'),
                ToastAndroid.SHORT
            );
            return false;
        }
        return true;
    }, [quantity, rangeMax, rangeMin, repetitionsAllowed, translate]);

    return (
        <OrientationHandlerView>
            <View
                style={
                    orientation === 'landscape'
                        ? globalStyles.flex
                        : globalStyles.noFlex
                }>
                <ScrollView scrollEnabled={orientation === 'landscape'}>
                    <View style={[globalStyles.row, styles.inputContainer]}>
                        <TextInput
                            style={[globalStyles.flex, styles.input]}
                            keyboardType="numeric"
                            mode="outlined"
                            label={translate('min')}
                            value={rangeMin}
                            onChangeText={setRangeMin}
                            contextMenuHidden
                            disableFullscreenUI
                        />
                        <TextInput
                            style={[globalStyles.flex, styles.input]}
                            keyboardType="numeric"
                            mode="outlined"
                            label={translate('max')}
                            value={rangeMax}
                            onChangeText={setRangeMax}
                            contextMenuHidden
                            disableFullscreenUI
                        />
                    </View>
                    <List.Item
                        title={`${translate('numberOfResults')}: ${quantity}`}
                    />
                    <Slider
                        value={initialSliderValue}
                        onValueChange={setQuantity}
                        minimumValue={1}
                        maximumValue={10}
                        step={1}
                        style={styles.slider}
                        accessibilityLabel="Number quantity"
                    />
                    <Divider />
                    <Checkbox.Item
                        label={translate('repetitions')}
                        status={repetitionsAllowed ? 'checked' : 'unchecked'}
                        onPress={toggleRepetitions}
                    />
                </ScrollView>
            </View>
            <ResultContainer<
                number[],
                {
                    sum: number;
                    average: string;
                }
            >
                animation={animations.fadeIn}
                showClipboardButton
                result={{
                    initialValue: [],
                    details: {
                        sum: res => res.reduce((acc, cur) => acc + cur, 0),
                        average: res =>
                            res.length > 0
                                ? (
                                      res.reduce((acc, cur) => acc + cur, 0) /
                                      res.length
                                  ).toFixed(2)
                                : '0',
                    },
                    beforeUpdate,
                    generate: () =>
                        Random.integers(
                            Number(rangeMin),
                            Number(rangeMax),
                            quantity,
                            !repetitionsAllowed
                        ),
                    isEmpty: res => res.length === 0,
                    processForClipboard: res => res.join(', '),
                }}
                fab={{
                    icon: 'shuffle',
                    label: translate('generateNumbers'),
                }}
                emptyIcon="numeric">
                {(result, details) => (
                    <>
                        <Text
                            variant="displayMedium"
                            style={[
                                styles.outputText,
                                {
                                    fontSize: moderateScale(
                                        result.join(', ').length <= 40 ? 30 : 25
                                    ),
                                },
                            ]}>
                            {result.join(', ')}
                        </Text>
                        {result.length > 1 && (
                            <>
                                <Text
                                    variant="bodySmall"
                                    style={styles.secondaryOutputText}>
                                    {translate('sum')} {details.sum}
                                </Text>
                                <Text
                                    variant="bodySmall"
                                    style={styles.secondaryOutputText}>
                                    {translate('average')} {details.average}
                                </Text>
                            </>
                        )}
                    </>
                )}
            </ResultContainer>
        </OrientationHandlerView>
    );
};

const styles = StyleSheet.create({
    inputContainer: {
        paddingHorizontal: 8,
    },
    input: {
        margin: 8,
    },
    slider: {
        width: moderateScale(200),
    },
    outputText: {
        textAlign: 'center',
        paddingVertical: 12,
        paddingHorizontal: 8,
    },
    secondaryOutputText: {
        textAlign: 'center',
        fontSize: moderateScale(12),
    },
});

export default RandomNumber;
