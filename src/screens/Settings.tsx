import React, {useCallback, useState, useMemo} from 'react';
import {StyleSheet, Platform, FlatList, ScrollView} from 'react-native';
import {
    List,
    Switch,
    Divider,
    ActivityIndicator,
    Modal,
    Portal,
    Surface,
    Dialog,
    RadioButton,
    Button,
    useTheme,
} from 'react-native-paper';
import {useLinkProps} from '@react-navigation/native';
import {useListsContext} from 'context/ListsContext';
import {useLocalization, StringKey} from 'context/LocalizationContext';
import {usePreferences, Theme} from 'context/PreferencesContext';
import useBooleanState from 'hooks/useBooleanState';
import useRequestAnimationFrame from 'hooks/useRequestAnimationFrame';
import {DrawerScreenRouteName, MainStackParamList} from 'navigation/types';
import globalStyles from 'utils/globalStyles';
import {listIcon} from 'utils/Utils';

const Settings = () => {
    const {translate} = useLocalization();
    const {theme, toggleTheme, initialScreen, setInitialScreen} =
        usePreferences();

    const [screenSelectionVisible, showScreenSelection, hideScreenSelection] =
        useBooleanState();
    const [loading, startLoading, stopLoading] = useBooleanState();

    const {
        actions: {importFromBackup, exportToBackup},
    } = useListsContext();

    const selectedScreenTitle = useMemo(
        () =>
            translate(
                screens.find(s => s.routeName === initialScreen)
                    ?.titleKey as StringKey
            ),
        [initialScreen, translate]
    );

    const {onPress: goToAbout} = useLinkProps<MainStackParamList>({
        to: {screen: 'About'},
    });

    const importLists = useRequestAnimationFrame(
        importFromBackup,
        startLoading,
        stopLoading
    );

    const exportLists = useRequestAnimationFrame(exportToBackup);

    const themeSwitch = useCallback(
        () => (
            <Switch value={theme === Theme.Dark} onValueChange={toggleTheme} />
        ),
        [theme, toggleTheme]
    );

    return (
        <>
            <ScrollView>
                <Surface style={styles.container} elevation={2}>
                    {Platform.Version < 29 && (
                        <List.Item
                            title={translate('darkTheme')}
                            left={listIcon('theme-light-dark')}
                            right={themeSwitch}
                            onPress={toggleTheme}
                        />
                    )}
                    <Divider />
                    <List.Item
                        title={translate('chooseInitialScreen')}
                        description={selectedScreenTitle}
                        left={listIcon('home-edit-outline')}
                        onPress={showScreenSelection}
                    />
                    <Divider />
                    <List.Item
                        title={translate('importListsFromBackup')}
                        titleNumberOfLines={2}
                        description={translate(
                            'yourExistingListsWillNotBeAffected'
                        )}
                        left={listIcon('application-import')}
                        onPress={importLists}
                    />
                    <List.Item
                        title={translate('exportListsToBackup')}
                        titleNumberOfLines={2}
                        left={listIcon('application-export')}
                        onPress={exportLists}
                    />
                    <Divider />
                    <List.Item
                        title={translate('about')}
                        left={listIcon('information-outline')}
                        onPress={goToAbout}
                    />
                </Surface>
            </ScrollView>
            <Portal>
                <Modal visible={loading} dismissable={false}>
                    <ActivityIndicator size="large" animating />
                </Modal>
                <ScreenSelectionDialog
                    title={translate('chooseInitialScreen')}
                    value={initialScreen}
                    onValueChange={setInitialScreen}
                    visible={screenSelectionVisible}
                    onDismiss={hideScreenSelection}
                />
            </Portal>
        </>
    );
};

interface ScreenData {
    titleKey: StringKey;
    routeName: DrawerScreenRouteName;
    icon: string;
}

const screens: Readonly<ScreenData[]> = [
    {
        titleKey: 'number',
        routeName: 'Number',
        icon: 'numeric',
    },
    {
        titleKey: 'coinFlip',
        routeName: 'CoinFlip',
        icon: 'hand-coin',
    },
    {
        titleKey: 'list',
        routeName: 'List',
        icon: 'format-list-bulleted',
    },
    {
        titleKey: 'letter',
        routeName: 'Letter',
        icon: 'alphabetical',
    },
    {
        titleKey: 'color',
        routeName: 'Color',
        icon: 'palette',
    },
    {
        titleKey: 'password',
        routeName: 'Password',
        icon: 'shield-account',
    },
    {
        titleKey: 'diceRoll',
        routeName: 'DiceRoll',
        icon: 'dice-multiple',
    },
    {
        titleKey: 'country',
        routeName: 'Country',
        icon: 'earth',
    },
    {
        titleKey: 'dateTime',
        routeName: 'DateTime',
        icon: 'calendar',
    },
    {
        titleKey: 'chemicalElement',
        routeName: 'ChemicalElement',
        icon: 'flask',
    },
];

const ScreenSelectionDialog = (props: {
    value: any;
    onValueChange: (route: DrawerScreenRouteName) => void;
    onDismiss: () => void;
    visible: boolean;
    title: string;
}) => {
    const theme = useTheme();
    const {translate} = useLocalization();

    const [tempValue, setTempValue] = useState(props.value);

    const onCancel = useCallback(() => {
        setTempValue(props.value);
        props.onDismiss();
    }, [props]);

    const onSubmit = useCallback(() => {
        props.onValueChange(tempValue);
        props.onDismiss();
    }, [props, tempValue]);

    const keyExtractor = useCallback((item: ScreenData) => item.titleKey, []);
    const renderItem = useCallback(
        ({item}: {item: ScreenData}) => (
            <RadioButton.Item
                label={translate(item.titleKey)}
                value={item.routeName}
                color={theme.colors.inversePrimary}
                uncheckedColor={theme.colors.outline}
            />
        ),
        [translate]
    );

    return (
        <Dialog
            visible={props.visible}
            onDismiss={onCancel}
            dismissable
            style={styles.dialog}>
            <Dialog.Title>{props.title}</Dialog.Title>
            <Dialog.ScrollArea>
                <RadioButton.Group
                    value={tempValue}
                    onValueChange={setTempValue}>
                    <FlatList
                        data={screens}
                        renderItem={renderItem}
                        keyExtractor={keyExtractor}
                    />
                </RadioButton.Group>
            </Dialog.ScrollArea>
            <Dialog.Actions>
                <Button
                    mode="text"
                    onPress={onCancel}
                    textColor={theme.colors.secondary}
                    style={globalStyles.dialogAction}>
                    {translate('cancel')}
                </Button>
                <Button
                    mode="text"
                    onPress={onSubmit}
                    textColor={theme.colors.secondary}
                    style={globalStyles.dialogAction}>
                    OK
                </Button>
            </Dialog.Actions>
        </Dialog>
    );
};

const styles = StyleSheet.create({
    container: {
        margin: 8,
        borderRadius: 4,
    },
    dialog: {
        maxHeight: '80%',
    },
});

export default React.memo(Settings);
