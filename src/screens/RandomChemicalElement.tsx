import React, {useCallback} from 'react';
import {StyleSheet} from 'react-native';
import {Text} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import Anchor from 'components/Anchor';
import ResultContainer from 'components/ResultContainer';
import {useLocalization, Locale} from 'context/LocalizationContext';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';

type ChemicalElementFullData = {
    names: Record<Locale, string>;
    symbol: string;
    wikipediaURL?: Partial<Record<Locale, string>>;
};

const ChemicalElements: ChemicalElementFullData[] = require('assets/strings/chemical-elements.json');

type ChemicalElement = {
    name: string;
    symbol: string;
    atomicNumber: number;
};

const RandomChemicalElement = () => {
    const {languageCode, translate} = useLocalization();

    const getName = useCallback(
        (n: number) => ChemicalElements[n - 1].names[languageCode],
        [languageCode]
    );

    const getURL = useCallback(
        ({atomicNumber}: ChemicalElement) =>
            ChemicalElements[atomicNumber - 1].wikipediaURL?.[languageCode] ??
            `https://${languageCode}.wikipedia.org/wiki/${
                ChemicalElements[atomicNumber - 1].names[languageCode]
            }`,
        [languageCode]
    );

    return (
        <ResultContainer<ChemicalElement, {wikipediaURL: string}>
            animation={animations.fadeIn}
            showClipboardButton
            result={{
                initialValue: {
                    name: '',
                    symbol: '',
                    atomicNumber: 0,
                },
                details: {
                    wikipediaURL: getURL,
                },
                generate: () => {
                    const atomicNumber = Random.integer(
                        1,
                        ChemicalElements.length
                    );
                    return {
                        atomicNumber,
                        name: getName(atomicNumber),
                        symbol: ChemicalElements[atomicNumber - 1].symbol,
                    };
                },
                isEmpty: res => res.name === '',
                processForClipboard: res => res.name,
            }}
            fab={{
                icon: 'shuffle',
                label: translate('generateChemicalElement'),
            }}
            emptyIcon="flask"
            emptyIconSize={80}>
            {(result, details) => (
                <>
                    <Text style={styles.resultDetailsContainer}>
                        <Text style={styles.resultAtomicNumber}>
                            {result.atomicNumber !== 0 && result.atomicNumber}
                        </Text>
                        <Text style={styles.resultSymbol}>{result.symbol}</Text>
                    </Text>
                    <Text style={styles.resultName}>{result.name}</Text>
                    {details.wikipediaURL !== undefined && (
                        <Anchor
                            icon="information"
                            href={details.wikipediaURL}
                            text={translate('viewOnWikipedia')}
                        />
                    )}
                </>
            )}
        </ResultContainer>
    );
};

const styles = StyleSheet.create({
    resultDetailsContainer: {
        textAlign: 'center',
        padding: moderateScale(10),
    },
    resultAtomicNumber: {
        fontSize: moderateScale(20),
        lineHeight: moderateScale(37),
    },
    resultSymbol: {
        fontSize: moderateScale(45),
    },
    resultName: {
        fontSize: moderateScale(25),
        textAlign: 'center',
        padding: moderateScale(10),
    },
});

export default RandomChemicalElement;
