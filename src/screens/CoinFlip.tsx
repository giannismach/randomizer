import React from 'react';
import {Image, StyleSheet} from 'react-native';
import {Text} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import ResultContainer from 'components/ResultContainer';
import {useLocalization, StringKey} from 'context/LocalizationContext';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';

type CoinSide = 'heads' | 'tails';

const CoinFlip = () => {
    const {translate} = useLocalization();

    return (
        <ResultContainer<CoinSide | ''>
            emptyIcon="hand-coin"
            emptyIconSize={80}
            animation={animations.flipInX}
            result={{
                initialValue: '',
                generate: () => (Random.boolean() ? 'heads' : 'tails'),
                isEmpty: result => result === '',
            }}
            fab={{
                icon: 'hand-coin',
                label: translate('flipACoin'),
            }}>
            {result => (
                <>
                    {result === 'heads' ? (
                        <Image
                            source={require('../assets/images/heads.webp')}
                            style={styles.image}
                        />
                    ) : result === 'tails' ? (
                        <Image
                            source={require('../assets/images/tails.webp')}
                            style={styles.image}
                        />
                    ) : null}
                    <Text style={styles.resultText}>
                        {translate(result.toLowerCase() as StringKey)}
                    </Text>
                </>
            )}
        </ResultContainer>
    );
};

const styles = StyleSheet.create({
    image: {
        width: moderateScale(160),
        height: moderateScale(160),
    },
    resultText: {
        textAlign: 'center',
        padding: moderateScale(10),
        fontSize: moderateScale(40),
    },
});

export default CoinFlip;
