import React, {useCallback} from 'react';
import {FlatList, StyleSheet} from 'react-native';
import {List, Surface, Text} from 'react-native-paper';
import {StringKey, useLocalization} from 'context/LocalizationContext';

interface TranslatorData {
    name: string;
    languageStringKey: StringKey;
}

const translators: TranslatorData[] = require('assets/strings/translators.json');

const Translators = () => {
    const {translate} = useLocalization();

    const keyExtractor = useCallback(
        (item: TranslatorData) => `${item.name} ${item.languageStringKey}`,
        []
    );

    const renderItem = useCallback(
        ({item}: {item: TranslatorData}) => {
            return (
                <List.Item
                    title={item.name}
                    description={translate(item.languageStringKey)}
                />
            );
        },
        [translate]
    );

    return (
        <FlatList
            data={translators}
            renderItem={renderItem}
            keyExtractor={keyExtractor}
            ListHeaderComponent={
                <Surface elevation={1} style={[styles.thankYouContainer]}>
                    <Text variant="bodyLarge" style={styles.thankYouText}>
                        {translate('translatorThanks')}
                    </Text>
                </Surface>
            }
        />
    );
};

const styles = StyleSheet.create({
    thankYouContainer: {
        paddingVertical: 12,
        paddingHorizontal: 6,
        margin: 8,
        borderRadius: 4,
    },
    thankYouText: {
        fontSize: 15,
        textAlign: 'center',
        paddingHorizontal: 8,
        paddingVertical: 4,
    },
});

export default React.memo(Translators);
