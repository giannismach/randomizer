import React, {useMemo, useState, useCallback} from 'react';
import {FlatList, View, StyleSheet} from 'react-native';
import {
    List,
    Text,
    IconButton,
    Searchbar,
    Divider,
    Button,
    useTheme,
} from 'react-native-paper';
import {useLinkTo} from '@react-navigation/native';
import MainFAB from 'components/MainFAB';
import {useLocalization} from 'context/LocalizationContext';
import {useListsContext} from 'context/ListsContext';
import {MainStackParamList} from 'navigation/types';
import globalStyles from 'utils/globalStyles';
import {ListWithItems} from 'utils/types';

const SelectList = () => {
    const linkTo = useLinkTo<MainStackParamList>();

    const theme = useTheme();
    const {
        data,
        actions: {importFromBackup},
    } = useListsContext();
    const {translate, isRTL} = useLocalization();

    const [searchQuery, setSearchQuery] = useState('');

    const showMultipleListModal = useCallback(() => {
        linkTo({screen: 'PickFromMultipleLists'});
    }, [linkTo]);

    const isEmpty = useMemo(() => data.length === 0, [data.length]);

    const filteredLists = useMemo(
        () =>
            data.filter(({list: {name}}) =>
                name.toLowerCase().includes(searchQuery.trim().toLowerCase())
            ),
        [data, searchQuery]
    );

    const goToCreateList = useCallback(() => {
        linkTo({screen: 'CreateList'});
    }, [linkTo]);

    const keyExtractor = useCallback(
        (item: ListWithItems) => item.list.id.toString(),
        []
    );

    const renderItem = useCallback(
        ({item}: {item: ListWithItems}) => {
            const elements = item.items.length;
            const onPress = () => {
                linkTo({
                    screen: 'ListMain',
                    params: {
                        listId: item.list.id,
                    },
                });
            };
            return (
                <List.Item
                    title={item.list.name}
                    description={`${elements} ${translate(
                        elements === 1 ? 'element' : 'elements'
                    )}`}
                    right={iconProps => (
                        <IconButton
                            {...iconProps}
                            icon={`chevron-${isRTL ? 'left' : 'right'}`}
                            onPress={onPress}
                        />
                    )}
                    onPress={onPress}
                />
            );
        },
        [isRTL, linkTo, translate]
    );

    const importLists = useCallback(
        () =>
            importFromBackup(
                () => {},
                () => {}
            ),
        [importFromBackup]
    );

    return (
        <>
            {isEmpty ? (
                <View style={globalStyles.centerWithFlex}>
                    <Text variant="titleLarge" style={styles.noListsCreatedYet}>
                        {translate('noListCreatedYet')}
                    </Text>
                    <View style={styles.buttonContainer}>
                        <Button
                            icon="plus"
                            mode="contained"
                            style={styles.defaultMargin}
                            onPress={goToCreateList}>
                            {translate('createList')}
                        </Button>
                        <Button
                            mode="outlined"
                            onPress={importLists}
                            style={styles.defaultMargin}>
                            {translate('importListsFromBackup')}
                        </Button>
                    </View>
                </View>
            ) : (
                <>
                    <Searchbar
                        placeholder={translate('search')}
                        style={styles.defaultMargin}
                        value={searchQuery}
                        onChangeText={setSearchQuery}
                        disableFullscreenUI
                    />
                    <Button
                        icon="set-merge"
                        mode="contained-tonal"
                        disabled={data.length < 2}
                        onPress={showMultipleListModal}
                        uppercase={false}
                        style={styles.defaultMargin}>
                        {translate('pickElementsFromMultipleLists')}
                    </Button>
                    <FlatList
                        data={filteredLists}
                        renderItem={renderItem}
                        keyExtractor={keyExtractor}
                        ItemSeparatorComponent={Divider}
                        ListEmptyComponent={
                            // appears when no lists match the search query
                            <View style={globalStyles.centerWithoutFlex}>
                                <Text
                                    variant="titleMedium"
                                    style={styles.noListsFoundSubheading}>
                                    {translate('noListsFound')}
                                </Text>
                            </View>
                        }
                    />
                </>
            )}
            <MainFAB icon="plus" visible={!isEmpty} onPress={goToCreateList} />
        </>
    );
};

const styles = StyleSheet.create({
    defaultMargin: {
        marginHorizontal: 16,
        marginBottom: 8,
        marginTop: 4
    },
    noListsCreatedYet: {
        textAlign: 'center',
        padding: 16,
    },
    noListsFoundSubheading: {
        padding: 8,
    },
    buttonContainer: {
        paddingVertical: 16,
        width: '100%',
    },
});

export default SelectList;
