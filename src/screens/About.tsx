import React, {useCallback} from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import {List, Divider, Surface} from 'react-native-paper';
import * as MailComposer from 'expo-mail-composer';
import {useLinkProps} from '@react-navigation/native';
import AppLogo from 'components/AppLogo';
import OrientationHandlerView from 'components/OrientationHandlerView';
import {useLocalization} from 'context/LocalizationContext';
import useScreenOrientation from 'hooks/useScreenOrientation';
import AppInfo from 'modules/AppInfo';
import {MainStackParamList} from 'navigation/types';
import WebBrowserUtils from 'utils/WebBrowserUtils';
import {listIcon} from 'utils/Utils';
import globalStyles from 'utils/globalStyles';

const privacyPolicy =
    'https://giannismacheras.github.io/randomizer-privacy-policy';
const email = 'giannismachairas.dev@gmail.com';

const AboutScreen = () => {
    const {translate} = useLocalization();

    const orientation = useScreenOrientation();

    const {onPress: goToLicenseInfo} = useLinkProps<MainStackParamList>({
        to: {screen: 'LicenseInfo'},
    });
    const {onPress: goToTranslators} = useLinkProps<MainStackParamList>({
        to: {screen: 'Translators'},
    });

    const openPrivacyPolicy = useCallback(() => {
        WebBrowserUtils.openBrowserWithFallback(privacyPolicy);
    }, []);

    const sendEmail = useCallback(() => {
        MailComposer.composeAsync({
            recipients: [email],
            subject: 'Randomizer',
        });
    }, []);

    return (
        <ScrollView>
            <OrientationHandlerView
                style={
                    orientation === 'landscape' &&
                    globalStyles.centerWithoutFlex
                }>
                <View style={orientation === 'landscape' && {minWidth: '35%'}}>
                    <AppLogo />
                </View>
                <Surface
                    elevation={2}
                    style={[
                        styles.container,
                        orientation === 'landscape' && globalStyles.flex,
                    ]}>
                    <List.Item
                        title={translate('version')}
                        description={AppInfo.appVersion}
                        left={listIcon('information-outline')}
                    />
                    <List.Item
                        title={translate('contact')}
                        description={translate('contactMe')}
                        descriptionNumberOfLines={4}
                        left={listIcon('email-outline')}
                        onPress={sendEmail}
                    />
                    <List.Item
                        title={translate('translations')}
                        left={listIcon('translate')}
                        onPress={goToTranslators}
                    />
                    <Divider />
                    <List.Item
                        title={translate('privacyPolicy')}
                        left={listIcon('shield-outline')}
                        onPress={openPrivacyPolicy}
                    />
                    <List.Item
                        title={translate('licenseInformation')}
                        left={listIcon('file-outline')}
                        onPress={goToLicenseInfo}
                    />
                </Surface>
            </OrientationHandlerView>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    container: {
        margin: 8,
        borderRadius: 4,
    },
});

export default React.memo(AboutScreen);
