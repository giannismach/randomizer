import React, {useCallback} from 'react';
import {StyleSheet, ToastAndroid, View} from 'react-native';
import {List, Divider, Text} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import DateTimePicker, {
    DateTimePickerEvent,
} from '@react-native-community/datetimepicker';
import OrientationHandlerView from 'components/OrientationHandlerView';
import ResultContainer from 'components/ResultContainer';
import Slider from 'components/Slider';
import {useLocalization} from 'context/LocalizationContext';
import useBooleanState from 'hooks/useBooleanState';
import useOption from 'hooks/useOption';
import useScreenOrientation from 'hooks/useScreenOrientation';
import globalStyles from 'utils/globalStyles';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';
import Converters from 'utils/Converters';
import Validators from 'utils/Validators';

const RandomTime = () => {
    const {translate} = useLocalization();

    const orientation = useScreenOrientation();

    const d = new Date();
    const [initialTime, setInitialTime] = useOption({
        defaultValue: new Date(
            d.getFullYear(),
            d.getMonth(),
            d.getDate(),
            0,
            0
        ),
        asyncStorageKey: '@Randomizer_RandomTime_initialTime',
        converter: Converters.stringToDate,
        validator: Validators.date,
    });
    const [finalTime, setFinalTime] = useOption({
        defaultValue: new Date(
            d.getFullYear(),
            d.getMonth(),
            d.getDate(),
            23,
            59
        ),
        asyncStorageKey: '@Randomizer_RandomTime_finalTime',
        converter: Converters.stringToDate,
        validator: Validators.date,
    });
    const [quantity, setQuantity, initialSliderValue] = useOption({
        defaultValue: 1,
        asyncStorageKey: '@Randomizer_RandomTime_quantity',
        converter: Converters.stringToNumber,
    });

    const [
        initialTimePickerVisible,
        showInitialTimePicker,
        hideInitialTimePicker,
    ] = useBooleanState();
    const [finalTimePickerVisible, showFinalTimePicker, hideFinalTimePicker] =
        useBooleanState();

    const onInitialTimeChange = useCallback(
        (_event: DateTimePickerEvent, time?: Date) => {
            hideInitialTimePicker();
            setInitialTime(t => time || t);
        },
        [hideInitialTimePicker, setInitialTime]
    );

    const onFinalTimeChange = useCallback(
        (_event: DateTimePickerEvent, time?: Date) => {
            hideFinalTimePicker();
            setFinalTime(t => time || t);
        },
        [hideFinalTimePicker, setFinalTime]
    );

    const beforeUpdate = useCallback(() => {
        if (finalTime.getTime() <= initialTime.getTime()) {
            ToastAndroid.show(
                translate('initialTimeEarlierThanFinalError'),
                ToastAndroid.SHORT
            );
            return false;
        }
        return true;
    }, [finalTime, initialTime, translate]);

    const formatTime = useCallback(
        (date: Date) =>
            date.toLocaleTimeString([], {
                hour: '2-digit',
                minute: '2-digit',
                hour12: false,
            }),
        []
    );

    const timeText = useCallback(
        (time: Date) => () =>
            <Text style={styles.timeText}>{formatTime(time)}</Text>,
        [formatTime]
    );

    return (
        <>
            <OrientationHandlerView>
                <View
                    style={
                        orientation === 'landscape'
                            ? globalStyles.flex
                            : globalStyles.noFlex
                    }>
                    <List.Item
                        title={`${translate('from')}: `}
                        right={timeText(initialTime)}
                        onPress={showInitialTimePicker}
                    />
                    <List.Item
                        title={`${translate('to')}: `}
                        right={timeText(finalTime)}
                        onPress={showFinalTimePicker}
                    />
                    <Divider />
                    <List.Item
                        title={`${translate('numberOfResults')}: ${quantity}`}
                    />
                    <Slider
                        value={initialSliderValue}
                        onValueChange={setQuantity}
                        minimumValue={1}
                        maximumValue={5}
                        step={1}
                        style={styles.slider}
                        accessibilityHint="Number of times"
                    />
                </View>
                <ResultContainer
                    animation={animations.fadeIn}
                    result={{
                        initialValue: [],
                        beforeUpdate,
                        generate: () =>
                            Array.from({length: quantity}).map(() =>
                                Random.date(initialTime, finalTime)
                            ),
                        isEmpty: res => res.length === 0,
                    }}
                    fab={{
                        icon: 'shuffle',
                        label: translate('generateTimes'),
                    }}
                    emptyIcon="clock-fast"
                    emptyIconSize={60}>
                    {result => (
                        <Text style={styles.outputText}>
                            {result.map(formatTime).join('\n')}
                        </Text>
                    )}
                </ResultContainer>
            </OrientationHandlerView>
            {initialTimePickerVisible && (
                <DateTimePicker
                    value={initialTime}
                    mode="time"
                    display="clock"
                    onChange={onInitialTimeChange}
                    is24Hour
                />
            )}
            {finalTimePickerVisible && (
                <DateTimePicker
                    value={finalTime}
                    mode="time"
                    display="clock"
                    onChange={onFinalTimeChange}
                    is24Hour
                />
            )}
        </>
    );
};

const styles = StyleSheet.create({
    slider: {
        width: moderateScale(150),
    },
    outputText: {
        fontSize: moderateScale(27),
        textAlign: 'center',
    },
    timeText: {
        textAlignVertical: 'center',
        paddingHorizontal: 5,
        fontSize: 16,
    },
});

export default RandomTime;
