import React from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import {Text, List} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import OrientationHandlerView from 'components/OrientationHandlerView';
import ResultContainer from 'components/ResultContainer';
import Slider from 'components/Slider';
import {useLocalization} from 'context/LocalizationContext';
import {useSelectedList} from 'context/SelectedListContext';
import useOption from 'hooks/useOption';
import useScreenOrientation from 'hooks/useScreenOrientation';
import globalStyles from 'utils/globalStyles';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';

const ListElement = () => {
    const {translate} = useLocalization();
    const {list, items} = useSelectedList();

    const orientation = useScreenOrientation();

    const [numberOfItems, setQuantity] = useOption<number>({
        defaultValue: 1,
    });

    const ref = React.useRef<{reset: () => void}>({
        reset: () => {},
    });

    React.useEffect(() => {
        ref.current.reset();
    }, [list.id]);

    return (
        <OrientationHandlerView>
            {items.length > 2 && (
                <View style={orientation === 'landscape' && {minWidth: '40%'}}>
                    <List.Item
                        title={`${translate(
                            'numberOfResults'
                        )}: ${numberOfItems}`}
                    />
                    <Slider
                        value={1}
                        onValueChange={setQuantity}
                        minimumValue={1}
                        maximumValue={Math.min(items.length, 10)}
                        step={1}
                        style={styles.slider}
                    />
                </View>
            )}
            <ResultContainer
                animation={animations.fadeIn}
                showClipboardButton
                result={{
                    initialValue: [],
                    generate: () =>
                        Random.arrayElements(items, numberOfItems).map(
                            ({item}) => item
                        ),

                    isEmpty: res => res.length === 0,
                    processForClipboard: res => res.join(', '),
                    ref,
                }}
                fab={{
                    icon: 'shuffle',
                    label: translate('pickElement'),
                }}
                emptyIcon="format-list-bulleted-type"
                emptyIconSize={70}>
                {result => (
                    <ScrollView
                        contentContainerStyle={[
                            globalStyles.centerWithoutFlex,
                            globalStyles.minHeightFull,
                        ]}>
                        <Text
                            style={[
                                styles.resultText,
                                {
                                    fontSize: moderateScale(
                                        result.join(',').length > 400
                                            ? 15
                                            : result.join(',').length > 100
                                            ? 20
                                            : result.join(',').length > 50
                                            ? 30
                                            : 40
                                    ),
                                },
                            ]}>
                            {result.join(',\n')}
                        </Text>
                    </ScrollView>
                )}
            </ResultContainer>
        </OrientationHandlerView>
    );
};

const styles = StyleSheet.create({
    slider: {
        width: moderateScale(200),
    },
    resultText: {
        textAlign: 'center',
        padding: 12,
    },
});

export default React.memo(ListElement);
