import React, {useCallback} from 'react';
import {StyleSheet, View} from 'react-native';
import {List, Text, Chip} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import OrientationHandlerView from 'components/OrientationHandlerView';
import ResultContainer from 'components/ResultContainer';
import Slider from 'components/Slider';
import {useLocalization} from 'context/LocalizationContext';
import useOption from 'hooks/useOption';
import useScreenOrientation from 'hooks/useScreenOrientation';
import Converters from 'utils/Converters';
import globalStyles from 'utils/globalStyles';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';

const RandomPassword = () => {
    const {translate} = useLocalization();

    const orientation = useScreenOrientation();

    const [passwordLength, setPasswordLength, initialSliderValue] = useOption({
        defaultValue: 8,
        asyncStorageKey: '@Randomizer_RandomPassword_passwordLength',
        converter: Converters.stringToNumber,
    });

    const [usingSymbols, setUsingSymbols] = useOption({
        defaultValue: true,
        asyncStorageKey: '@Randomizer_RandomPassword_usingSymbols',
        converter: Converters.stringToBoolean,
    });

    const [usingLowerCase, setUsingLowerCase] = useOption({
        defaultValue: true,
        asyncStorageKey: '@Randomizer_RandomPassword_usingLowerCase',
        converter: Converters.stringToBoolean,
    });

    const [usingUpperCase, setUsingUpperCase] = useOption({
        defaultValue: true,
        asyncStorageKey: '@Randomizer_RandomPassword_usingUpperCase',
        converter: Converters.stringToBoolean,
    });

    const [usingDigits, setUsingDigits] = useOption({
        defaultValue: true,
        asyncStorageKey: '@Randomizer_RandomPassword_usingDigits',
        converter: Converters.stringToBoolean,
    });

    const toggleSymbols = useCallback(
        () => setUsingSymbols(u => !u),
        [setUsingSymbols]
    );

    const toggleUpperCase = useCallback(() => {
        setUsingUpperCase(u => !u);
    }, [setUsingUpperCase]);

    const toggleLowerCase = useCallback(() => {
        setUsingLowerCase(u => !u);
    }, [setUsingLowerCase]);

    const toggleDigits = useCallback(() => {
        setUsingDigits(u => !u);
    }, [setUsingDigits]);

    return (
        <OrientationHandlerView>
            <View
                style={
                    orientation === 'landscape'
                        ? globalStyles.flex
                        : globalStyles.noFlex
                }>
                <List.Item
                    title={`${translate('passwordLength')} ${passwordLength}`}
                />
                <Slider
                    value={initialSliderValue}
                    onValueChange={setPasswordLength}
                    minimumValue={4}
                    maximumValue={32}
                    step={1}
                    style={styles.slider}
                    accessibilityLabel="Password length"
                />
                <View style={[globalStyles.inlineContainer, {padding: 8}]}>
                    <Chip
                        selected={usingSymbols}
                        onPress={toggleSymbols}
                        style={styles.optionChip}
                        showSelectedOverlay
                        disabled={
                            !usingLowerCase && !usingUpperCase && !usingDigits
                        }>
                        {translate('symbols')}
                    </Chip>
                    <Chip
                        selected={usingLowerCase}
                        onPress={toggleLowerCase}
                        style={styles.optionChip}
                        showSelectedOverlay
                        disabled={
                            !usingSymbols && !usingUpperCase && !usingDigits
                        }>
                        {translate('lowercaseLetters')}
                    </Chip>
                    <Chip
                        selected={usingUpperCase}
                        onPress={toggleUpperCase}
                        style={styles.optionChip}
                        showSelectedOverlay
                        disabled={
                            !usingSymbols && !usingLowerCase && !usingDigits
                        }>
                        {translate('uppercaseLetters')}
                    </Chip>
                    <Chip
                        selected={usingDigits}
                        onPress={toggleDigits}
                        style={styles.optionChip}
                        showSelectedOverlay
                        disabled={
                            !usingSymbols && !usingLowerCase && !usingUpperCase
                        }>
                        {translate('digits')}
                    </Chip>
                </View>
            </View>
            <ResultContainer
                animation={animations.fadeIn}
                showClipboardButton
                result={{
                    initialValue: '',
                    beforeUpdate: () =>
                        usingSymbols ||
                        usingLowerCase ||
                        usingUpperCase ||
                        usingDigits,
                    generate: () => {
                        const lowerCase = 'abcdefghijklmnopqrstuvwxyz';
                        const upperCase = lowerCase.toUpperCase();
                        const digits = '0123456789';
                        const symbols = '!#$%&()*+-/?@^_~';
                        const allChars =
                            (usingLowerCase ? lowerCase : '') +
                            (usingUpperCase ? upperCase : '') +
                            (usingDigits ? digits : '') +
                            (usingSymbols ? symbols : '');
                        return Random.charsFromString(allChars, passwordLength);
                    },
                    isEmpty: res => res === '',
                    processForClipboard: res => res,
                }}
                fab={{
                    icon: 'shuffle',
                    label: translate('generatePassword'),
                }}
                emptyIcon="shield-account"
                emptyIconSize={70}>
                {password => <Text style={styles.outputText}>{password}</Text>}
            </ResultContainer>
        </OrientationHandlerView>
    );
};

const styles = StyleSheet.create({
    slider: {
        width: moderateScale(250),
    },
    optionChip: {
        margin: 8,
    },
    outputText: {
        fontSize: moderateScale(25),
        textAlign: 'center',
        padding: 2,
    },
});

export default RandomPassword;
