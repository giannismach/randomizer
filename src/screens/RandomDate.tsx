import React, {useCallback} from 'react';
import {StyleSheet, ToastAndroid, View, ScrollView} from 'react-native';
import {List, Divider, Text} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import DateTimePicker, {
    DateTimePickerEvent,
} from '@react-native-community/datetimepicker';
import ResultContainer from 'components/ResultContainer';
import Slider from 'components/Slider';
import {useLocalization} from 'context/LocalizationContext';
import useBooleanState from 'hooks/useBooleanState';
import useOption from 'hooks/useOption';
import useScreenOrientation from 'hooks/useScreenOrientation';
import globalStyles from 'utils/globalStyles';
import Random from 'utils/Random';
import Converters from 'utils/Converters';
import Validators from 'utils/Validators';
import {animations} from 'utils/Utils';
import OrientationHandlerView from 'components/OrientationHandlerView';

const MIN_DATE = new Date(1000, 0, 1);
const MAX_DATE = new Date(2400, 0, 1);

const RandomDate = () => {
    const {translate} = useLocalization();

    const orientation = useScreenOrientation();

    const _d = new Date();
    _d.setMonth(new Date().getMonth() - 1);
    const [initialDate, setInitialDate] = useOption({
        defaultValue: _d,
        asyncStorageKey: '@Randomizer_RandomDate_initialDate',
        converter: Converters.stringToDate,
        validator: Validators.date,
    });
    const [finalDate, setFinalDate] = useOption({
        defaultValue: new Date(),
        asyncStorageKey: '@Randomizer_RandomDate_finalDate',
        converter: Converters.stringToDate,
        validator: Validators.date,
    });
    const [quantity, setQuantity, initialSliderValue] = useOption({
        defaultValue: 1,
        asyncStorageKey: '@Randomizer_RandomDate_quantity',
        converter: Converters.stringToNumber,
    });
    const [
        initialDatePickerVisible,
        showInitialDatePicker,
        hideInitialDatePicker,
    ] = useBooleanState();
    const [finalDatePickerVisible, showFinalDatePicker, hideFinalDatePicker] =
        useBooleanState();

    const onInitialDateChange = (_event: DateTimePickerEvent, date?: Date) => {
        hideInitialDatePicker();
        setInitialDate(d => date || d);
    };

    const onFinalDateChange = (_event: DateTimePickerEvent, date?: Date) => {
        hideFinalDatePicker();
        setFinalDate(d => date || d);
    };

    const beforeUpdate = useCallback(() => {
        if (finalDate.getTime() <= initialDate.getTime()) {
            ToastAndroid.show(
                translate('initialDateEarlierThanFinalError'),
                ToastAndroid.SHORT
            );
            return false;
        }
        return true;
    }, [finalDate, initialDate, translate]);

    const formatDate = useCallback(
        (date: Date) => date.toLocaleDateString(),
        []
    );

    const dateText = useCallback(
        (date: Date) => () =>
            <Text style={styles.dateText}>{formatDate(date)}</Text>,
        [formatDate]
    );

    return (
        <>
            <OrientationHandlerView>
                <View
                    style={
                        orientation === 'landscape'
                            ? globalStyles.flex
                            : globalStyles.noFlex
                    }>
                    <ScrollView scrollEnabled={orientation === 'landscape'}>
                        <List.Item
                            title={`${translate('from')}: `}
                            right={dateText(initialDate)}
                            onPress={showInitialDatePicker}
                        />
                        <List.Item
                            title={`${translate('to')}: `}
                            right={dateText(finalDate)}
                            onPress={showFinalDatePicker}
                        />
                        <Divider />
                        <List.Item
                            title={`${translate(
                                'numberOfResults'
                            )}: ${quantity}`}
                        />
                        <Slider
                            value={initialSliderValue}
                            onValueChange={setQuantity}
                            minimumValue={1}
                            maximumValue={5}
                            step={1}
                            style={styles.slider}
                            accessibilityHint="Number of dates"
                        />
                    </ScrollView>
                </View>
                <ResultContainer
                    animation={animations.fadeIn}
                    result={{
                        initialValue: [],
                        beforeUpdate,
                        generate: () =>
                            Array.from({length: quantity}).map(() =>
                                Random.date(initialDate, finalDate)
                            ),
                        isEmpty: res => res.length === 0,
                    }}
                    fab={{
                        icon: 'shuffle',
                        label: translate('generateDates'),
                    }}
                    emptyIcon="calendar-question"
                    emptyIconSize={60}>
                    {result => (
                        <Text style={styles.outputText}>
                            {result
                                .map(date => date.toLocaleDateString())
                                .join('\n')}
                        </Text>
                    )}
                </ResultContainer>
            </OrientationHandlerView>
            {initialDatePickerVisible && (
                <DateTimePicker
                    value={initialDate}
                    minimumDate={MIN_DATE}
                    maximumDate={MAX_DATE}
                    mode="date"
                    display="calendar"
                    onChange={onInitialDateChange}
                />
            )}
            {finalDatePickerVisible && (
                <DateTimePicker
                    value={finalDate}
                    minimumDate={MIN_DATE}
                    maximumDate={MAX_DATE}
                    mode="date"
                    display="calendar"
                    onChange={onFinalDateChange}
                />
            )}
        </>
    );
};

const styles = StyleSheet.create({
    slider: {
        width: moderateScale(150),
    },
    outputText: {
        fontSize: moderateScale(24),
        textAlign: 'center',
        padding: 2,
    },
    dateText: {
        textAlignVertical: 'center',
        paddingHorizontal: 5,
        fontSize: 16,
    },
});

export default RandomDate;
