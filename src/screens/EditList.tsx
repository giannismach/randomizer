import React from 'react';
import {StyleSheet, FlatList, View} from 'react-native';
import {IconButton, List, Surface, useTheme} from 'react-native-paper';
import Snackbar from 'react-native-snackbar';
import EditDialog from 'components/EditDialog';
import ListItemInput from 'components/ListItemInput';
import OrientationHandlerView from 'components/OrientationHandlerView';
import {useListsContext} from 'context/ListsContext';
import {useLocalization} from 'context/LocalizationContext';
import {useSelectedList} from 'context/SelectedListContext';
import useBooleanState from 'hooks/useBooleanState';
import useScreenOrientation from 'hooks/useScreenOrientation';
import useSorted from 'hooks/useSorted';
import globalStyles from 'utils/globalStyles';
import {ListItem} from 'utils/types';

const ListItemView = React.memo(
    ({
        item,
        onEdit,
        onDelete,
        numberOfItems,
    }: {
        item: ListItem;
        onEdit: (i: ListItem) => void;
        onDelete: (i: ListItem) => void;
        numberOfItems: number;
    }) => {
        const editItem = React.useCallback(() => {
            onEdit(item);
        }, [item, onEdit]);

        const deleteItem = React.useCallback(() => {
            onDelete(item);
        }, [item, onDelete]);

        return (
            <List.Item
                title={item.item}
                titleNumberOfLines={6}
                right={iconProps => (
                    <>
                        <IconButton
                            icon="pencil-outline"
                            onPress={editItem}
                            {...iconProps}
                        />
                        <IconButton
                            icon="close"
                            onPress={deleteItem}
                            accessibilityLabel="Delete item"
                            disabled={numberOfItems === 1}
                            {...iconProps}
                        />
                    </>
                )}
            />
        );
    }
);

const EditList = () => {
    const {colors} = useTheme();
    const {translate} = useLocalization();

    const screenOrientation = useScreenOrientation();

    const {data: lists} = useListsContext();
    const {list, items, actions} = useSelectedList();

    const listItemInputRef = React.useRef();

    const sortedItems = useSorted(items, i => i.item);

    const [nameDialogVisible, showNameDialog, hideNameDialog] =
        useBooleanState();

    const [editedListItem, setEditedListItem] = React.useState<ListItem | null>(
        null
    );
    const resetEditedListItem = React.useCallback(
        () => setEditedListItem(null),
        []
    );

    const addItem = React.useCallback(
        (item: string) => {
            if (item.trim().length > 0) {
                actions.addItem(item);
                Snackbar.dismiss();
            }
        },
        [actions]
    );

    const deleteItem = React.useCallback(
        (item: ListItem) => {
            const _item = item.item;
            actions.deleteItem(item.id);
            Snackbar.show({
                text: translate('itemDeleted'),
                backgroundColor: colors.inverseSurface,
                action: {
                    text: translate('undo'),
                    textColor: colors.inversePrimary,
                    onPress: () => {
                        actions.addItem(_item);
                    },
                },
            });
        },
        [actions, colors.secondary, translate]
    );

    const validateListName = React.useCallback(
        (name: string): string | true => {
            if (
                lists.some(({list: l}) => l.name === name && l.id !== list.id)
            ) {
                return translate('duplicateListNameError');
            }
            if (name.trim() === '') {
                return '';
            }
            return true;
        },
        [lists, translate, list.id]
    );

    const validateListItem = React.useCallback(
        (item: string) => (item.trim() === '' ? '' : true),
        []
    );
    const editItem = React.useCallback(
        (item: string) => {
            if (editedListItem) {
                if (item.trim() !== '') {
                    actions.updateItem(editedListItem.id, item);
                }
            }
        },
        [actions, editedListItem]
    );

    const itemKeyExtractor = React.useCallback(item => item.id.toString(), []);
    const renderListItem = React.useCallback(
        ({item}) => (
            <ListItemView
                item={item}
                numberOfItems={items.length}
                onEdit={setEditedListItem}
                onDelete={deleteItem}
            />
        ),
        [deleteItem, items.length]
    );

    React.useEffect(() => {
        listItemInputRef.current.reset();
        resetEditedListItem();
    }, [list.id, resetEditedListItem]);

    return (
        <>
            <OrientationHandlerView>
                <View
                    style={[
                        globalStyles.column,
                        screenOrientation === 'landscape'
                            ? globalStyles.flex
                            : globalStyles.noFlex,
                    ]}>
                    <Surface style={styles.listNameContainer} elevation={1}>
                        <List.Item
                            title={list.name}
                            description={`${items.length} ${translate(
                                items.length === 1 ? 'element' : 'elements'
                            )}`}
                            right={props => (
                                <IconButton
                                    {...props}
                                    icon="pencil-outline"
                                    onPress={showNameDialog}
                                />
                            )}
                        />
                    </Surface>
                    <ListItemInput ref={listItemInputRef} submit={addItem} />
                </View>
                <View style={globalStyles.flex}>
                    <FlatList
                        data={sortedItems}
                        renderItem={renderListItem}
                        keyExtractor={itemKeyExtractor}
                    />
                </View>
            </OrientationHandlerView>
            <EditDialog
                visible={nameDialogVisible}
                onDismiss={hideNameDialog}
                initialValue={list.name}
                title={translate('editName')}
                inputLabel={translate('listName')}
                onSubmit={actions.updateName}
                validate={validateListName}
            />
            {editedListItem !== null && (
                <EditDialog
                    title={translate('editItem')}
                    visible
                    onDismiss={resetEditedListItem}
                    initialValue={editedListItem.item}
                    validate={validateListItem}
                    onSubmit={editItem}
                />
            )}
        </>
    );
};

const styles = StyleSheet.create({
    listNameContainer: {
        marginTop: 8,
        marginHorizontal: 16,
        borderRadius: 4,
    },
});

export default EditList;
