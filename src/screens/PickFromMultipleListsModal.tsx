import React, {useState, useCallback, useRef} from 'react';
import {FlatList, View, StatusBar, StyleSheet} from 'react-native';
import {List, IconButton, Divider, Text} from 'react-native-paper';
import {NativeStackNavigationProp} from '@react-navigation/native-stack';
import PagerView from 'react-native-pager-view';
import ListMultipleSelection from 'components/ListMultipleSelection';
import MainFAB from 'components/MainFAB';
import ResultContainer from 'components/ResultContainer';
import {useListsContext} from 'context/ListsContext';
import {useLocalization} from 'context/LocalizationContext';
import {MainStackParamList} from 'navigation/types';
import globalStyles from 'utils/globalStyles';
import Random from 'utils/Random';
import {animations, listIcon} from 'utils/Utils';

enum Page {
    ListSelection,
    ItemPicker,
}
type ItemPickerResult = {
    item: string;
    itemId: number;
    listName: string;
};

const PickFromMultipleListsModal = ({
    navigation,
}: {
    navigation: NativeStackNavigationProp<
        MainStackParamList,
        'PickFromMultipleLists'
    >;
}) => {
    const {translate} = useLocalization();
    const {getListFromId} = useListsContext();
    const [selectedLists, setSelectedLists] = useState<number[]>([]);

    const pagerRef = useRef<PagerView>();

    const setPage = useCallback((page: Page) => {
        pagerRef.current?.setPage(page);
    }, []);
    const goToListSelection = useCallback(
        () => setPage(Page.ListSelection),
        [setPage]
    );
    const goToItemPicker = useCallback(
        () => setPage(Page.ItemPicker),
        [setPage]
    );

    const onDismiss = useCallback(() => {
        navigation.goBack();
    }, [navigation]);

    const renderPickedItem = ({item}: {item: ItemPickerResult}) => (
        <List.Item title={item.item} description={item.listName} />
    );
    const pickedItemKeyExtractor = ({itemId}: ItemPickerResult) =>
        itemId.toString();

    return (
        <View style={globalStyles.flex}>
            <IconButton
                icon="close"
                onPress={onDismiss}
                style={styles.iconButton}
            />
            <PagerView
                initialPage={Page.ListSelection}
                scrollEnabled={false}
                // @ts-ignore
                ref={pagerRef}
                style={globalStyles.flex}>
                <View key={Page.ListSelection}>
                    <Text variant="titleMedium" style={styles.title}>
                        {translate('selectLists')}
                    </Text>
                    <Text
                        variant="bodyMedium"
                        style={styles.explanationParagraph}>
                        {translate('pickFromMultipleListsExplanation')}
                    </Text>
                    <Divider />
                    <ListMultipleSelection
                        selectedLists={selectedLists}
                        setSelectedLists={setSelectedLists}
                    />
                    <MainFAB
                        icon="arrow-right"
                        label={translate('next')}
                        onPress={goToItemPicker}
                        visible={selectedLists.length >= 2}
                        // disabled={selectedLists.length < 2}
                        uppercase={false}
                    />
                </View>
                <View key={Page.ItemPicker}>
                    <Text variant="titleMedium" style={styles.title}>
                        {translate('pickElements')}
                    </Text>
                    <List.Item
                        title={`${translate('selectedLists')} ${selectedLists
                            .map(getListFromId)
                            .map(({list: {name}}) => name)
                            .join(', ')}`}
                        titleNumberOfLines={10}
                        left={listIcon('format-list-bulleted')}
                        onPress={goToListSelection}
                    />
                    <Divider />
                    <ResultContainer<ItemPickerResult[]>
                        result={{
                            initialValue: [],
                            isEmpty: val => val.length === 0,
                            generate: () => {
                                const result: ItemPickerResult[] = [];
                                for (let listId of selectedLists) {
                                    const {list, items} = getListFromId(listId);
                                    const randomItem =
                                        Random.arrayElement(items);
                                    result.push({
                                        item: randomItem.item,
                                        itemId: randomItem.id,
                                        listName: list.name,
                                    });
                                }
                                return result;
                            },
                        }}
                        animation={animations.fadeIn}
                        emptyIcon="format-list-bulleted-square"
                        noSurface
                        noPadding
                        noCenter
                        fab={{
                            icon: 'set-merge',
                            label: translate('pickElements'),
                            uppercase: false,
                        }}>
                        {res => (
                            <FlatList
                                data={res}
                                renderItem={renderPickedItem}
                                keyExtractor={pickedItemKeyExtractor}
                            />
                        )}
                    </ResultContainer>
                </View>
            </PagerView>
        </View>
    );
};

const styles = StyleSheet.create({
    iconButton: {
        position: 'absolute',
        right: 8,
        top: StatusBar.currentHeight,
        zIndex: 1,
    },
    title: {
        paddingStart: 16,
        paddingTop: (StatusBar.currentHeight as number) + 16,
        paddingBottom: 16,
    },
    explanationParagraph: {
        paddingHorizontal: 16,
        paddingBottom: 16,
    },
});

export default React.memo(PickFromMultipleListsModal);
