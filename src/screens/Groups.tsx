import React from 'react';
import {View, StyleSheet, FlatList, ToastAndroid} from 'react-native';
import {
    Text,
    Button,
    List,
    IconButton,
    Divider,
    useTheme,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import OrientationHandlerView from 'components/OrientationHandlerView';
import ResultContainer from 'components/ResultContainer';
import Slider from 'components/Slider';
import {useLocalization} from 'context/LocalizationContext';
import {useSelectedList} from 'context/SelectedListContext';
import useScreenOrientation from 'hooks/useScreenOrientation';
import Clipboard from 'modules/Clipboard';
import {ListMainTabsNavigationProp} from 'navigation/types';
import globalStyles from 'utils/globalStyles';
import Random from 'utils/Random';
import {ListItem} from 'utils/types';
import {animations} from 'utils/Utils';

interface Props {
    navigation: ListMainTabsNavigationProp<'Groups'>;
}

const Groups = (props: Props) => {
    const theme = useTheme();
    const {translate} = useLocalization();
    const {list, items} = useSelectedList();

    const orientation = useScreenOrientation();

    const sliderMaxValue = React.useMemo(
        () => Math.min(10, items.length),
        [items.length]
    );

    const isLengthTooSmall = React.useMemo(
        () => items.length <= 2,
        [items.length]
    );

    const [quantity, setQuantity] = React.useState(2);

    const ref = React.useRef<{reset: () => void}>({
        reset: () => {},
    });

    React.useEffect(() => {
        if (isLengthTooSmall) {
            ref.current.reset();
        }
    }, [isLengthTooSmall]);

    React.useEffect(() => {
        ref.current.reset();
    }, [list.id]);

    const editList = React.useCallback(() => {
        props.navigation.navigate('Edit', {});
    }, [props.navigation]);

    const renderItem = React.useCallback(
        ({item}: {item: ListItem[]}) => <GroupItem group={item} />,
        []
    );

    return (
        <>
            {isLengthTooSmall && (
                <View style={globalStyles.centerWithFlex}>
                    <Icon
                        name="alert"
                        size={75}
                        color={theme.colors.onBackground}
                    />
                    <Text variant="titleLarge" style={styles.title}>
                        {translate('listTooSmallForGroups')}
                    </Text>
                    <Button
                        mode="contained"
                        icon="pencil"
                        onPress={editList}
                        style={styles.button}>
                        {translate('editList')}
                    </Button>
                </View>
            )}
            <OrientationHandlerView
                style={{display: isLengthTooSmall ? 'none' : 'flex'}}>
                <View style={orientation === 'landscape' && {minWidth: '40%'}}>
                    <List.Item
                        title={`${translate('numberOfGroups')} ${quantity}`}
                    />
                    {/* this is necessary because Slider keeps appearing after display change */}
                    {!isLengthTooSmall && (
                        <Slider
                            minimumValue={2}
                            maximumValue={sliderMaxValue}
                            onValueChange={setQuantity}
                            value={2}
                            step={1}
                            style={{
                                width: Math.max(150, sliderMaxValue * 20),
                            }}
                        />
                    )}
                </View>
                <ResultContainer
                    animation={animations.fadeIn}
                    result={{
                        initialValue: [],
                        generate: () => Random.splitInGroups(items, quantity),
                        isEmpty: res => res.length === 0,
                        ref,
                    }}
                    fab={{
                        icon: 'account-multiple',
                        label: translate('createGroups'),
                        visible: !isLengthTooSmall,
                    }}
                    noCenter
                    noPadding
                    emptyIcon="account-multiple-outline">
                    {result => (
                        <FlatList
                            data={result}
                            renderItem={renderItem}
                            ItemSeparatorComponent={Divider}
                        />
                    )}
                </ResultContainer>
            </OrientationHandlerView>
        </>
    );
};

const GroupItem = React.memo<{group: ListItem[]}>(({group}) => {
    const {translate} = useLocalization();

    const groupStringified = React.useMemo(
        () => group.map(i => i.item).join(', '),
        [group]
    );

    const copyGroup = React.useCallback(() => {
        Clipboard.setString(groupStringified);
        ToastAndroid.show(translate('copiedToClipboard'), ToastAndroid.SHORT);
    }, [groupStringified, translate]);

    return (
        <List.Item
            title={groupStringified}
            titleNumberOfLines={8}
            right={iconProps => (
                <IconButton
                    {...iconProps}
                    icon="content-copy"
                    onPress={copyGroup}
                />
            )}
        />
    );
});

const styles = StyleSheet.create({
    title: {
        textAlign: 'center',
        padding: 12,
    },
    button: {
        margin: 12,
    },
});

export default React.memo(Groups);
