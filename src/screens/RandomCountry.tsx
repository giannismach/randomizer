import React, {useCallback} from 'react';
import {StyleSheet} from 'react-native';
import {Text} from 'react-native-paper';
import {moderateScale} from 'react-native-size-matters';
import ResultContainer from 'components/ResultContainer';
import Anchor from 'components/Anchor';
import {useLocalization, Locale} from 'context/LocalizationContext';
import Random from 'utils/Random';
import {animations} from 'utils/Utils';

const countries: Record<Locale, string[]> = {
    en: require('assets/strings/countryLists/en.json'),
    el: require('assets/strings/countryLists/el.json'),
    es: require('assets/strings/countryLists/es.json'),
    pt: require('assets/strings/countryLists/pt.json'),
    ru: require('assets/strings/countryLists/ru.json'),
    bg: require('assets/strings/countryLists/bg.json'),
    fr: require('assets/strings/countryLists/fr.json'),
    he: require('assets/strings/countryLists/he.json'),
    de: require('assets/strings/countryLists/de.json'),
    pl: require('assets/strings/countryLists/pl.json'),
};

const wikipediaUrlExceptions: Partial<Record<Locale, Record<string, string>>> =
    {
        fr: {
            // prettier-ignore
            'Équateur': 'https://fr.wikipedia.org/wiki/%C3%89quateur_(pays)',
        },
        pl: {
            // prettier-ignore
            'Palestyna': 'https://pl.wikipedia.org/wiki/Palestyna_(pa%C5%84stwo)',
        },
    };

const RandomCountry = () => {
    const {languageCode, translate} = useLocalization();

    const getURL = useCallback(
        (country: string) =>
            wikipediaUrlExceptions?.[languageCode]?.[country] ??
            `https://${languageCode}.wikipedia.org/wiki/${country}`,
        [languageCode]
    );

    return (
        <ResultContainer<string, {wikipediaURL: string}>
            animation={animations.fadeIn}
            showClipboardButton
            result={{
                initialValue: '',
                generate: () => Random.arrayElement(countries[languageCode]),
                details: {
                    wikipediaURL: getURL,
                },
                isEmpty: res => res === '',
                processForClipboard: res => res,
            }}
            fab={{
                icon: 'shuffle',
                label: translate('generateCountry'),
            }}
            emptyIcon="earth"
            emptyIconSize={80}>
            {(res, details) => (
                <>
                    <Text style={styles.countryName}>{res}</Text>
                    {details.wikipediaURL !== undefined && (
                        <Anchor
                            icon="information"
                            href={details.wikipediaURL}
                            text={translate('viewOnWikipedia')}
                        />
                    )}
                </>
            )}
        </ResultContainer>
    );
};

const styles = StyleSheet.create({
    countryName: {
        fontSize: moderateScale(40),
        textAlign: 'center',
        padding: 24,
    },
});

export default RandomCountry;
