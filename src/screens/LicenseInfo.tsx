import React, {useCallback} from 'react';
import {FlatList} from 'react-native';
import {List, Divider} from 'react-native-paper';
import WebBrowserUtils from 'utils/WebBrowserUtils';

import librariesJSON from 'assets/strings/licenses.json';

type LibraryName = keyof typeof librariesJSON;

interface LibraryData {
    library: LibraryName;
    licenses: string;
    repository: string;
    licenseFile?: string;
    licenseUrl: string;
    parents: string;
}

const libraries: LibraryData[] = Object.keys(librariesJSON).map(
    (key: string) => ({
        library: key as LibraryName,
        ...librariesJSON[key as LibraryName],
    })
);

const LicenseInfo = () => {
    const openURL = useCallback(
        url => () => {
            WebBrowserUtils.openBrowserWithFallback(url);
        },
        []
    );

    const keyExtractor = useCallback((item: LibraryData) => item.library, []);
    const renderLicenseListItem = useCallback(
        ({item}: {item: LibraryData}) => {
            return (
                <List.Item
                    title={item.library}
                    onPress={openURL(item.licenseUrl)}
                />
            );
        },
        [openURL]
    );

    return (
        <FlatList
            data={libraries}
            renderItem={renderLicenseListItem}
            keyExtractor={keyExtractor}
            initialNumToRender={15}
            ItemSeparatorComponent={Divider}
        />
    );
};

export default React.memo(LicenseInfo);
