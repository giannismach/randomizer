import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    pickerContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
    },
    pickerCaption: {
        fontSize: 16,
        paddingHorizontal: 16,
    },
    inlineContainer: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
    },
    centerWithFlex: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
    },
    centerWithoutFlex: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    flex: {
        flex: 1,
    },
    noFlex: {
        flex: 0,
    },
    row: {
        flexDirection: 'row',
    },
    column: {
        flexDirection: 'column',
    },
    noElevation: {
        elevation: 0,
    },
    minHeightFull: {
        minHeight: '100%',
    },
    dialogAction: {
        marginHorizontal: 8,
    },
});
