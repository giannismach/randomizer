import {ListWithItems, TemporaryListItem} from 'utils/types';

namespace Actions {
    export interface AddItem {
        type: ReducerActionType.ADD;
        data: {
            item: string;
        };
    }

    export interface DeleteItem {
        type: ReducerActionType.DELETE;
        data: {
            item: TemporaryListItem;
        };
    }

    export interface AppendList {
        type: ReducerActionType.APPEND;
        data: {
            list: ListWithItems;
        };
    }

    export interface UndoChange {
        type: ReducerActionType.UNDO_CHANGE;
    }
}

type ReducerAction =
    | Actions.AddItem
    | Actions.DeleteItem
    | Actions.AppendList
    | Actions.UndoChange;
export enum ReducerActionType {
    ADD = 'ADD',
    DELETE = 'DELETE',
    APPEND = 'APPEND',
    UNDO_CHANGE = 'UNDO_CHANGE',
}

type Items = TemporaryListItem[];

type ItemsWithPrevious = [Items, Items];

const reducer = (
    [data, previousData]: ItemsWithPrevious,
    action: ReducerAction
): ItemsWithPrevious => {
    switch (action.type) {
        case ReducerActionType.ADD:
            if (action.data.item.trim() === '') {
                return [data, previousData];
            }
            return [
                data.concat([
                    {key: new Date().toString(), item: action.data.item},
                ]),
                data,
            ];
        case ReducerActionType.APPEND:
            return [
                data.concat(
                    action.data.list.items.map(item => ({
                        item: item.item,
                        listId: item.listId,
                        key: `${item.id} ${new Date().toString()}`,
                    }))
                ),
                data,
            ];
        case ReducerActionType.DELETE: {
            let itemsCopy = [...data];
            itemsCopy.splice(
                itemsCopy.findIndex(i => i.key === action.data.item.key),
                1
            );
            return [itemsCopy, data];
        }
        case ReducerActionType.UNDO_CHANGE:
            return [previousData, previousData];
        default:
            return [data, previousData];
    }
};

export default reducer;
