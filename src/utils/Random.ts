namespace Random {
    export const integer = (min: number, max: number): number => {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    const randomSet = <T>(generator: () => T, setSize: number) => {
        let result = new Set<T>();
        while (result.size < setSize) {
            result.add(generator());
        }
        return Array.from(result);
    };

    export const integers = (
        min: number,
        max: number,
        quantity: number,
        unique: boolean = false
    ): number[] => {
        if (quantity < 1) {
            throw Error('Quantity must be bigger than 1.');
        }

        if (unique) {
            if (max - min + 1 < quantity) {
                throw Error('Too small range'); // should never be thrown, is checked in RandomNumber
            } else {
                return randomSet(() => Random.integer(min, max), quantity);
            }
        } else {
            return Array.from({length: quantity}).map(() =>
                Random.integer(min, max)
            );
        }
    };

    export const boolean = (): boolean => Math.round(Math.random()) === 1;

    export const date = (min: Date, max: Date): Date => {
        return new Date(Random.integer(min.getTime(), max.getTime()));
    };

    export const shuffle = <T>(array: T[]): T[] => {
        // using Fisher-Yates shuffle
        let newArray = [...array];
        for (let i = array.length - 1; i > 0; i--) {
            let j = Random.integer(0, i);
            [newArray[i], newArray[j]] = [newArray[j], newArray[i]];
        }
        return newArray;
    };

    export const splitInGroups = <T>(
        array: T[],
        groupNumber: number
    ): T[][] => {
        const shuffledArray = Random.shuffle(array);
        let result = [];
        for (let i = groupNumber; i > 0; i--) {
            result.push(
                shuffledArray.splice(0, Math.ceil(shuffledArray.length / i))
            );
        }
        return result;
    };

    export const arrayElement = <T>(array: T[]): T =>
        array[Random.integer(0, array.length - 1)];

    export const arrayElements = <T>(array: T[], numberOfElements: number) =>
        randomSet(() => Random.arrayElement(array), numberOfElements);

    export const charFromString = (string: string): string =>
        string[Random.integer(0, string.length - 1)];

    export const charsFromString = (
        string: string,
        quantity: number
    ): string => {
        if (quantity < 1) {
            throw Error('At least one character must be generated.');
        }
        let result = '';
        for (let i = 0; i < quantity; i++) {
            result += Random.charFromString(string);
        }
        return result;
    };

    export namespace colors {
        export const hexadecimal = (): string =>
            `#${Random.charsFromString('0123456789ABCDEF', 6)}`;

        export const HSL = (): string =>
            // prettier-ignore
            `hsl(${Random.integer(0, 360 - 1)}, ${Random.integer(0, 100)}%, ${Random.integer(0, 100)}%)`;

        export const RGB = (): string =>
            // prettier-ignore
            `rgb(${Random.integer(0, 255)}, ${Random.integer(0, 255)}, ${Random.integer(0, 255)})`;
    }
}

export default Random;
