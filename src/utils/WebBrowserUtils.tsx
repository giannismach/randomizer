import {useEffect} from 'react';
import {Linking} from 'react-native';
import * as WebBrowser from 'expo-web-browser';

namespace WebBrowserUtils {
    export const useWarmUp = () => {
        useEffect(() => {
            WebBrowser.warmUpAsync();
            return () => {
                WebBrowser.coolDownAsync();
            };
        }, []);
    };

    export const useMayInitWithUrl = (url: string) => {
        useEffect(() => {
            if (url) {
                WebBrowser.mayInitWithUrlAsync(url);
            }
        }, [url]);
    };

    export const openBrowserWithFallback = (
        url: string,
        options?: WebBrowser.WebBrowserOpenOptions
    ) => {
        WebBrowser.getCustomTabsSupportingBrowsersAsync().then(browsers => {
            return browsers.servicePackages.length > 0
                ? WebBrowser.openBrowserAsync(url, options)
                : Linking.openURL(url);
        });
    };
}

export default WebBrowserUtils;
