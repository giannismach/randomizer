import {ColorType} from 'screens/RandomColor';

const ColorTypes: Readonly<ColorType[]> = ['hex', 'hsl', 'rgb'];

type Validator<T> = (val: T) => boolean;
export type StringValidator = Validator<string>;

namespace Validators {
    export const date: StringValidator = str =>
        !Number.isNaN(new Date(str).getFullYear());
    export const colorType: StringValidator = str =>
        ColorTypes.indexOf(str as ColorType) !== -1;
}

export default Validators;
