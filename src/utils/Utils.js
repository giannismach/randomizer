import React from 'react';
import {List} from 'react-native-paper';

const listIcon = icon => props => <List.Icon icon={icon} {...props} />;

const animations = {
    fadeIn: (ref, duration) => ref.current.fadeIn(duration ?? 400),
    flipInX: (ref, duration) => ref.current.flipInX(duration ?? 400),
};

export {listIcon, animations};
