import React from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {useFocusEffect, useNavigation} from '@react-navigation/native';

// Show a badge for newly added tabs, in order to inform users about new features
// The badge should not be shown after the user has used the feature (visited the tab)

export const useShouldShowTabBadge = (key: string) => {
    const [showBadge, setShowBadge] = React.useState(false);

    React.useEffect(() => {
        AsyncStorage.getItem(key).then(res => {
            if (!res) {
                setShowBadge(true);
            }
        });
    }, [key]);

    return showBadge;
};

export const useTabBadgeManagement = (key: string) => {
    const navigation = useNavigation();
    useFocusEffect(
        React.useCallback(() => {
            AsyncStorage.setItem(key, JSON.stringify('true'));
            navigation.setOptions({
                tabBarBadge: false,
            });
        }, [key, navigation])
    );
};
