import React from 'react';

const contextWithNullSecurity = <T>(context: React.Context<T | null>) => {
    const useSafeContext = () => {
        const contextValue = React.useContext(context);
        if (contextValue === null) {
            throw new Error(
                `Attempt to use ${context.displayName} outside a provider`
            );
        }
        return contextValue;
    };
    return useSafeContext;
};

export default contextWithNullSecurity;
