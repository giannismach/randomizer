// List-related
interface ListItemCommon {
    item: string;
}

export interface TemporaryListItem extends ListItemCommon {
    key: string; // date if manually added, date + id if imported from another list
}

export type ListID = number;
export type ListItemID = number;

export interface ListItem extends ListItemCommon {
    id: ListItemID;
    listId: ListID;
}

export interface List {
    id: ListID;
    name: string;
}

export interface ListWithItems {
    list: List;
    items: ListItem[];
}

export interface ListActions {
    remove: () => void;
    addItem: (item: string) => void;
    updateItem: (id: number, item: string) => void;
    deleteItem: (itemId: number) => void;
    updateName: (newName: string) => void;
}
