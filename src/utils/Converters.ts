export type Converter<I, O> = (i: I) => O;
export type StringConverter<O> = Converter<string, O>;

namespace Converters {
    export const stringToNumber: StringConverter<number> = str => Number(str);
    export const stringToDate: StringConverter<Date> = str => new Date(str);
    export const stringToBoolean: StringConverter<boolean> = str =>
        JSON.parse(str);
}

export default Converters;
