from googletrans import Translator
import wikipediaapi
import json
import os

translator = Translator()

source_language = input('Source language: ')
dest_language = input('Destination language: ')
path_to_input = input('Path to JSON input file: ')
assert(os.path.exists(path_to_input))
path_to_output = input('Path to output file: ')

wikipedia = wikipediaapi.Wikipedia(dest_language)

with open(path_to_input) as json_file:
    json_data = json.load(json_file)

valid_translations = []
failed_translations = []
invalid_translations = []
for country in json_data:
    try:
        translation = translator.translate(country, dest=dest_language, src=source_language)
        wikipedia_page_exists = wikipedia.page(translation.text).exists()
        if wikipedia_page_exists: 
            valid_translations.append(translation.text)
            # print(f"{translation.origin} -> {translation.text}")
        else:
            invalid_translations.append(translation.origin)
    except:
        print(f'Translating {country} failed')
        failed_translations.append(country)

with open(path_to_output, 'w') as outfile:
    json.dump(valid_translations, outfile, ensure_ascii=False)
    print(f'{len(valid_translations)} translations written to {outfile.name}')

print('Failed translations: ', failed_translations)
print('Invalid translations: ', invalid_translations)
