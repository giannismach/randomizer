from googletrans import Translator
import wikipediaapi
import json
import os

translator = Translator()

source_language = input('Source language: ')
dest_language = input('Destination language: ')
path_to_json = input('Path to JSON file: ')

wikipedia = wikipediaapi.Wikipedia(dest_language)

assert(os.path.exists(path_to_json))

with open(path_to_json) as json_file:
    json_data = json.load(json_file)

failed_translations = []
invalid_translations = []
for element in json_data:
    try:
        translation = translator.translate(element['names'][source_language], dest=dest_language, src=source_language)
        wikipedia_page_exists = wikipedia.page(translation.text).exists()
        if wikipedia_page_exists: 
            element['names'][dest_language] = translation.text
        else:
            invalid_translations.append(translation.origin)
        # print(f"{translation.origin} -> {translation.text}")
    except:
        failed_translations.append(element['names'][source_language])

with open(path_to_json, 'w') as outfile:
    json.dump(json_data, outfile, ensure_ascii=False)

print('Failed translations: ', failed_translations)
print('Invalid translations: ', invalid_translations)
